// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class NoConstructorClass extends ClassDefSec {

    private ClassDefThrd ClassDefThrd;

    public NoConstructorClass (ClassDefThrd ClassDefThrd) {
        this.ClassDefThrd=ClassDefThrd;
        if(ClassDefThrd!=null) ClassDefThrd.setParent(this);
    }

    public ClassDefThrd getClassDefThrd() {
        return ClassDefThrd;
    }

    public void setClassDefThrd(ClassDefThrd ClassDefThrd) {
        this.ClassDefThrd=ClassDefThrd;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassDefThrd!=null) ClassDefThrd.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassDefThrd!=null) ClassDefThrd.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassDefThrd!=null) ClassDefThrd.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("NoConstructorClass(\n");

        if(ClassDefThrd!=null)
            buffer.append(ClassDefThrd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [NoConstructorClass]");
        return buffer.toString();
    }
}
