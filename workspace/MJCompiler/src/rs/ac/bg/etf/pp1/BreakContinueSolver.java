package rs.ac.bg.etf.pp1;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import rs.etf.pp1.mj.runtime.Code;

public class BreakContinueSolver {

	public enum OP_TYPE {
		BREAK,
		CONTINUE
	}
	
	public static class OpInfo {
		public OP_TYPE opType;
		public int adr;
		
		public OpInfo( OP_TYPE opType, int adr ) {
			this.opType = opType;
			this.adr = adr;
		}
	}
	
	private Stack<List<OpInfo>> opStack = new Stack<List<OpInfo>>();
	
	public void doStarted() {
		opStack.push(new ArrayList<OpInfo>());
	}
	
	public void addOp(OP_TYPE opType, int adr) {
		opStack.peek().add(new OpInfo(opType, adr));
	}
	
	public void solveDo(int conditionAdr, int exitAdr) {
		List<OpInfo> list = opStack.pop();
		for (OpInfo opInfo : list) {
			if( opInfo.opType == OP_TYPE.BREAK ) {
				Code.put2(opInfo.adr, exitAdr - opInfo.adr + 1);
			} else {
				Code.put2(opInfo.adr, conditionAdr - opInfo.adr + 1);
			}
		}
	}
}
