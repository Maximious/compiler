// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class ArrayRefList extends ArrRef {

    private ArrRef ArrRef;
    private Expr Expr;

    public ArrayRefList (ArrRef ArrRef, Expr Expr) {
        this.ArrRef=ArrRef;
        if(ArrRef!=null) ArrRef.setParent(this);
        this.Expr=Expr;
        if(Expr!=null) Expr.setParent(this);
    }

    public ArrRef getArrRef() {
        return ArrRef;
    }

    public void setArrRef(ArrRef ArrRef) {
        this.ArrRef=ArrRef;
    }

    public Expr getExpr() {
        return Expr;
    }

    public void setExpr(Expr Expr) {
        this.Expr=Expr;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ArrRef!=null) ArrRef.accept(visitor);
        if(Expr!=null) Expr.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ArrRef!=null) ArrRef.traverseTopDown(visitor);
        if(Expr!=null) Expr.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ArrRef!=null) ArrRef.traverseBottomUp(visitor);
        if(Expr!=null) Expr.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ArrayRefList(\n");

        if(ArrRef!=null)
            buffer.append(ArrRef.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr!=null)
            buffer.append(Expr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ArrayRefList]");
        return buffer.toString();
    }
}
