// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class ConstructorVarDeclaration extends ConstructorDecl {

    private String nm;
    private VarDeclList VarDeclList;
    private ConstructorDeclSec ConstructorDeclSec;

    public ConstructorVarDeclaration (String nm, VarDeclList VarDeclList, ConstructorDeclSec ConstructorDeclSec) {
        this.nm=nm;
        this.VarDeclList=VarDeclList;
        if(VarDeclList!=null) VarDeclList.setParent(this);
        this.ConstructorDeclSec=ConstructorDeclSec;
        if(ConstructorDeclSec!=null) ConstructorDeclSec.setParent(this);
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm=nm;
    }

    public VarDeclList getVarDeclList() {
        return VarDeclList;
    }

    public void setVarDeclList(VarDeclList VarDeclList) {
        this.VarDeclList=VarDeclList;
    }

    public ConstructorDeclSec getConstructorDeclSec() {
        return ConstructorDeclSec;
    }

    public void setConstructorDeclSec(ConstructorDeclSec ConstructorDeclSec) {
        this.ConstructorDeclSec=ConstructorDeclSec;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclList!=null) VarDeclList.accept(visitor);
        if(ConstructorDeclSec!=null) ConstructorDeclSec.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclList!=null) VarDeclList.traverseTopDown(visitor);
        if(ConstructorDeclSec!=null) ConstructorDeclSec.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclList!=null) VarDeclList.traverseBottomUp(visitor);
        if(ConstructorDeclSec!=null) ConstructorDeclSec.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ConstructorVarDeclaration(\n");

        buffer.append(" "+tab+nm);
        buffer.append("\n");

        if(VarDeclList!=null)
            buffer.append(VarDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConstructorDeclSec!=null)
            buffer.append(ConstructorDeclSec.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ConstructorVarDeclaration]");
        return buffer.toString();
    }
}
