// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class ArrayRefListAlloc extends ArrRefAlloc {

    private ArrRefAlloc ArrRefAlloc;
    private Expr Expr;

    public ArrayRefListAlloc (ArrRefAlloc ArrRefAlloc, Expr Expr) {
        this.ArrRefAlloc=ArrRefAlloc;
        if(ArrRefAlloc!=null) ArrRefAlloc.setParent(this);
        this.Expr=Expr;
        if(Expr!=null) Expr.setParent(this);
    }

    public ArrRefAlloc getArrRefAlloc() {
        return ArrRefAlloc;
    }

    public void setArrRefAlloc(ArrRefAlloc ArrRefAlloc) {
        this.ArrRefAlloc=ArrRefAlloc;
    }

    public Expr getExpr() {
        return Expr;
    }

    public void setExpr(Expr Expr) {
        this.Expr=Expr;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ArrRefAlloc!=null) ArrRefAlloc.accept(visitor);
        if(Expr!=null) Expr.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ArrRefAlloc!=null) ArrRefAlloc.traverseTopDown(visitor);
        if(Expr!=null) Expr.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ArrRefAlloc!=null) ArrRefAlloc.traverseBottomUp(visitor);
        if(Expr!=null) Expr.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ArrayRefListAlloc(\n");

        if(ArrRefAlloc!=null)
            buffer.append(ArrRefAlloc.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr!=null)
            buffer.append(Expr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ArrayRefListAlloc]");
        return buffer.toString();
    }
}
