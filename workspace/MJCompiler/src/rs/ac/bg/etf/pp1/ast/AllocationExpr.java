// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class AllocationExpr extends Factor {

    private Type Type;
    private ArrRefAlloc ArrRefAlloc;

    public AllocationExpr (Type Type, ArrRefAlloc ArrRefAlloc) {
        this.Type=Type;
        if(Type!=null) Type.setParent(this);
        this.ArrRefAlloc=ArrRefAlloc;
        if(ArrRefAlloc!=null) ArrRefAlloc.setParent(this);
    }

    public Type getType() {
        return Type;
    }

    public void setType(Type Type) {
        this.Type=Type;
    }

    public ArrRefAlloc getArrRefAlloc() {
        return ArrRefAlloc;
    }

    public void setArrRefAlloc(ArrRefAlloc ArrRefAlloc) {
        this.ArrRefAlloc=ArrRefAlloc;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Type!=null) Type.accept(visitor);
        if(ArrRefAlloc!=null) ArrRefAlloc.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Type!=null) Type.traverseTopDown(visitor);
        if(ArrRefAlloc!=null) ArrRefAlloc.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Type!=null) Type.traverseBottomUp(visitor);
        if(ArrRefAlloc!=null) ArrRefAlloc.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AllocationExpr(\n");

        if(Type!=null)
            buffer.append(Type.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ArrRefAlloc!=null)
            buffer.append(ArrRefAlloc.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AllocationExpr]");
        return buffer.toString();
    }
}
