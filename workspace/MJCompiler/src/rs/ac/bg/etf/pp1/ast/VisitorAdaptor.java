// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public abstract class VisitorAdaptor implements Visitor { 

    public void visit(Unmatched Unmatched) { }
    public void visit(MethodDefStart MethodDefStart) { }
    public void visit(ClassDefSec ClassDefSec) { }
    public void visit(RecDecl RecDecl) { }
    public void visit(ProgramMet ProgramMet) { }
    public void visit(MethodDecl MethodDecl) { }
    public void visit(Arr Arr) { }
    public void visit(ConstructorDecl ConstructorDecl) { }
    public void visit(Matched Matched) { }
    public void visit(Relop Relop) { }
    public void visit(MethodRetType MethodRetType) { }
    public void visit(PossDecl PossDecl) { }
    public void visit(RefDot RefDot) { }
    public void visit(StatementList StatementList) { }
    public void visit(ClassDeclSec ClassDeclSec) { }
    public void visit(DoWhileStart DoWhileStart) { }
    public void visit(Factor Factor) { }
    public void visit(CondTerm CondTerm) { }
    public void visit(AddExprList AddExprList) { }
    public void visit(ConstList ConstList) { }
    public void visit(DeclList DeclList) { }
    public void visit(ClassDef ClassDef) { }
    public void visit(Designator Designator) { }
    public void visit(Term Term) { }
    public void visit(MethodDef MethodDef) { }
    public void visit(Condition Condition) { }
    public void visit(Statements Statements) { }
    public void visit(AssignExpr AssignExpr) { }
    public void visit(TermExpr TermExpr) { }
    public void visit(IfCondition IfCondition) { }
    public void visit(FactorDesPar FactorDesPar) { }
    public void visit(MulFactor MulFactor) { }
    public void visit(ArrRef ArrRef) { }
    public void visit(VarDeclList VarDeclList) { }
    public void visit(ConstVal ConstVal) { }
    public void visit(Expr Expr) { }
    public void visit(MethodDeclSec MethodDeclSec) { }
    public void visit(ArrRefAlloc ArrRefAlloc) { }
    public void visit(ActPars ActPars) { }
    public void visit(DesignatorStatement DesignatorStatement) { }
    public void visit(Statement Statement) { }
    public void visit(VarDecl VarDecl) { }
    public void visit(VarParamList VarParamList) { }
    public void visit(ClassDecl ClassDecl) { }
    public void visit(ConstDecl ConstDecl) { }
    public void visit(CondFact CondFact) { }
    public void visit(ConstructorDeclSec ConstructorDeclSec) { }
    public void visit(MethodDeclList MethodDeclList) { }
    public void visit(FormParsDecl FormParsDecl) { }
    public void visit(ClassDefThrd ClassDefThrd) { }
    public void visit(FormPars FormPars) { }
    public void visit(Moduop Moduop) { visit(); }
    public void visit(Divop Divop) { visit(); }
    public void visit(Mulop Mulop) { visit(); }
    public void visit(Subop Subop) { visit(); }
    public void visit(Addop Addop) { visit(); }
    public void visit(LessEqualThan LessEqualThan) { visit(); }
    public void visit(LessThan LessThan) { visit(); }
    public void visit(GreatherEqualThan GreatherEqualThan) { visit(); }
    public void visit(GreatherThan GreatherThan) { visit(); }
    public void visit(NotEqual NotEqual) { visit(); }
    public void visit(RelEqual RelEqual) { visit(); }
    public void visit(Assignop Assignop) { visit(); }
    public void visit(NoArrayRef NoArrayRef) { visit(); }
    public void visit(ArrayRefList ArrayRefList) { visit(); }
    public void visit(ReferencingDot ReferencingDot) { visit(); }
    public void visit(DesName DesName) { visit(); }
    public void visit(SimpleDesignator SimpleDesignator) { visit(); }
    public void visit(SimpleDesignatorClass SimpleDesignatorClass) { visit(); }
    public void visit(DesignatorClass DesignatorClass) { visit(); }
    public void visit(NoFactorDesignatorParActs NoFactorDesignatorParActs) { visit(); }
    public void visit(FactorDesignatorParActs FactorDesignatorParActs) { visit(); }
    public void visit(NoArrayRefAlloc NoArrayRefAlloc) { visit(); }
    public void visit(ArrayRefListAlloc ArrayRefListAlloc) { visit(); }
    public void visit(ParenExpression ParenExpression) { visit(); }
    public void visit(AllocationExpr AllocationExpr) { visit(); }
    public void visit(ConstBoolFactor ConstBoolFactor) { visit(); }
    public void visit(ConstCharFactor ConstCharFactor) { visit(); }
    public void visit(ConstNumFactor ConstNumFactor) { visit(); }
    public void visit(DesignatorFactorNoParams DesignatorFactorNoParams) { visit(); }
    public void visit(DesignatorFactorParams DesignatorFactorParams) { visit(); }
    public void visit(MulopFactorSingle MulopFactorSingle) { visit(); }
    public void visit(MulopFactor MulopFactor) { visit(); }
    public void visit(TermNoMulopList TermNoMulopList) { visit(); }
    public void visit(TermMulopList TermMulopList) { visit(); }
    public void visit(SingleAddExpressionList SingleAddExpressionList) { visit(); }
    public void visit(AddExpressionList AddExpressionList) { visit(); }
    public void visit(MinusTermExpr MinusTermExpr) { visit(); }
    public void visit(TermExpression TermExpression) { visit(); }
    public void visit(ExpressionNoAddList ExpressionNoAddList) { visit(); }
    public void visit(ExpressionAddList ExpressionAddList) { visit(); }
    public void visit(CondFactNoRelation CondFactNoRelation) { visit(); }
    public void visit(CondFactRelation CondFactRelation) { visit(); }
    public void visit(BasicCondFact BasicCondFact) { visit(); }
    public void visit(ConditionTerm ConditionTerm) { visit(); }
    public void visit(BasicCondTerm BasicCondTerm) { visit(); }
    public void visit(ConditionClass ConditionClass) { visit(); }
    public void visit(SingleActPar SingleActPar) { visit(); }
    public void visit(ActParsClass ActParsClass) { visit(); }
    public void visit(ErrAssignment ErrAssignment) { visit(); }
    public void visit(AssignExprClass AssignExprClass) { visit(); }
    public void visit(MinusMinus MinusMinus) { visit(); }
    public void visit(PlusPlus PlusPlus) { visit(); }
    public void visit(CallProcEmpty CallProcEmpty) { visit(); }
    public void visit(CallProc CallProc) { visit(); }
    public void visit(Assign Assign) { visit(); }
    public void visit(EmptyStatementsClass EmptyStatementsClass) { visit(); }
    public void visit(StatementsClass StatementsClass) { visit(); }
    public void visit(ErrorCondition ErrorCondition) { visit(); }
    public void visit(IfConditionClass IfConditionClass) { visit(); }
    public void visit(DoStart DoStart) { visit(); }
    public void visit(ElseCond ElseCond) { visit(); }
    public void visit(WhileStart WhileStart) { visit(); }
    public void visit(IfStart IfStart) { visit(); }
    public void visit(IfElseUnmatched IfElseUnmatched) { visit(); }
    public void visit(IfUnmatched IfUnmatched) { visit(); }
    public void visit(PrintNoConst PrintNoConst) { visit(); }
    public void visit(PrintConst PrintConst) { visit(); }
    public void visit(Read Read) { visit(); }
    public void visit(IfElseMatched IfElseMatched) { visit(); }
    public void visit(ReturnNoExpr ReturnNoExpr) { visit(); }
    public void visit(ReturnExpr ReturnExpr) { visit(); }
    public void visit(Do Do) { visit(); }
    public void visit(Continue Continue) { visit(); }
    public void visit(Break Break) { visit(); }
    public void visit(StatementStatements StatementStatements) { visit(); }
    public void visit(DesignatorStment DesignatorStment) { visit(); }
    public void visit(UnmatchedClass UnmatchedClass) { visit(); }
    public void visit(MatchedClass MatchedClass) { visit(); }
    public void visit(SingleStmt SingleStmt) { visit(); }
    public void visit(StatementListClass StatementListClass) { visit(); }
    public void visit(FormalParamDecl FormalParamDecl) { visit(); }
    public void visit(FormalParamDeclError FormalParamDeclError) { visit(); }
    public void visit(NoFormalParamDecl NoFormalParamDecl) { visit(); }
    public void visit(FormalParamDeclMany FormalParamDeclMany) { visit(); }
    public void visit(FromParamsError FromParamsError) { visit(); }
    public void visit(FromParamsDecl FromParamsDecl) { visit(); }
    public void visit(NoFormPars NoFormPars) { visit(); }
    public void visit(VoidClass VoidClass) { visit(); }
    public void visit(MethodReturnType MethodReturnType) { visit(); }
    public void visit(MethodTypeName MethodTypeName) { visit(); }
    public void visit(NoMethodDefinition NoMethodDefinition) { visit(); }
    public void visit(MethodDefinition MethodDefinition) { visit(); }
    public void visit(MethodDefinitionStart MethodDefinitionStart) { visit(); }
    public void visit(NoMethodDeclVars NoMethodDeclVars) { visit(); }
    public void visit(MethodDeclVars MethodDeclVars) { visit(); }
    public void visit(MethodDeclaration MethodDeclaration) { visit(); }
    public void visit(SingleMethodDecl SingleMethodDecl) { visit(); }
    public void visit(MethodDeclarations MethodDeclarations) { visit(); }
    public void visit(EmptyClass EmptyClass) { visit(); }
    public void visit(ClassStatements ClassStatements) { visit(); }
    public void visit(ConstructorNoVarDeclaration ConstructorNoVarDeclaration) { visit(); }
    public void visit(ConstructorVarDeclaration ConstructorVarDeclaration) { visit(); }
    public void visit(ClassName ClassName) { visit(); }
    public void visit(NoClassMethods NoClassMethods) { visit(); }
    public void visit(ClassMethods ClassMethods) { visit(); }
    public void visit(NoConstructorClass NoConstructorClass) { visit(); }
    public void visit(ConstructorClass ConstructorClass) { visit(); }
    public void visit(NoClassDefinition NoClassDefinition) { visit(); }
    public void visit(ClassDefinition ClassDefinition) { visit(); }
    public void visit(ClassNoVarDeclaration ClassNoVarDeclaration) { visit(); }
    public void visit(ClassVarDeclaration ClassVarDeclaration) { visit(); }
    public void visit(ClassNoExtend ClassNoExtend) { visit(); }
    public void visit(ClassExtend ClassExtend) { visit(); }
    public void visit(RecordName RecordName) { visit(); }
    public void visit(RecordNoDeclarationsList RecordNoDeclarationsList) { visit(); }
    public void visit(RecordDeclarationsList RecordDeclarationsList) { visit(); }
    public void visit(ConstChar ConstChar) { visit(); }
    public void visit(ConstNum ConstNum) { visit(); }
    public void visit(ConstBool ConstBool) { visit(); }
    public void visit(SingleConst SingleConst) { visit(); }
    public void visit(ConstListClass ConstListClass) { visit(); }
    public void visit(ConstantDeclaration ConstantDeclaration) { visit(); }
    public void visit(Type Type) { visit(); }
    public void visit(NoArrDecl NoArrDecl) { visit(); }
    public void visit(Array Array) { visit(); }
    public void visit(VarParamListError VarParamListError) { visit(); }
    public void visit(VarParamListStart VarParamListStart) { visit(); }
    public void visit(VarParamListClass VarParamListClass) { visit(); }
    public void visit(VarDeclarationError VarDeclarationError) { visit(); }
    public void visit(VarDeclarationMany VarDeclarationMany) { visit(); }
    public void visit(VarDeclarationSingle VarDeclarationSingle) { visit(); }
    public void visit(SingleVarDeclaration SingleVarDeclaration) { visit(); }
    public void visit(VarDeclarations VarDeclarations) { visit(); }
    public void visit(PossClassDeclaration PossClassDeclaration) { visit(); }
    public void visit(PossRecDeclaration PossRecDeclaration) { visit(); }
    public void visit(PossConstDeclaration PossConstDeclaration) { visit(); }
    public void visit(PossVarDeclaration PossVarDeclaration) { visit(); }
    public void visit(NoDeclaration NoDeclaration) { visit(); }
    public void visit(Declarations Declarations) { visit(); }
    public void visit(NoProgramMethodDeclarations NoProgramMethodDeclarations) { visit(); }
    public void visit(ProgramMethodDeclarations ProgramMethodDeclarations) { visit(); }
    public void visit(ProgramName ProgramName) { visit(); }
    public void visit(Program Program) { visit(); }


    public void visit() { }
}
