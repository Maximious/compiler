package rs.ac.bg.etf.pp1;

import java.util.Collection;
import java.util.Stack;

import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.*;
import rs.etf.pp1.symboltable.structure.SymbolDataStructure;
import rs.etf.pp1.symboltable.visitors.DumpSymbolTableVisitor;

public class SemanticAnalyzer extends VisitorAdaptor {

    public SemanticAnalyzer() {
	// dodati i tip bool
	Tab.insert( Obj.Type, "bool", new Struct( Struct.Bool ) );
    }

    public int numOfRecords;
    public int numOfMethods;
    public int numOfGlobals;
    public int numOfConsts;
    public int numOfGlobalArrs;
    public int numOfMainLocals;
    public int numOfMainStatements;
    public int numOfMainFuncCalls;

    boolean errorDetected = false;
    Obj currentMethod = null;
    Stack<Obj> currentMethodCall = new Stack<Obj>();
    int numberOfParams = 0;
    Obj currentRecord = null;
    boolean returnFound = false;
    int doWhileCounter = 0;
    int nVars;

    Logger log = Logger.getLogger( getClass() );

    // metode za ispis stanja
    public void report_error( String message, SyntaxNode info ) {
	errorDetected = true;
	StringBuilder msg = new StringBuilder( message );
	int line = ( info == null ) ? 0 : info.getLine();
	if ( line != 0 )
	    msg.append( " na liniji " ).append( line );
	log.error( msg.toString() );
    }

    public void report_info( String message, SyntaxNode info ) {
	StringBuilder msg = new StringBuilder( message );
	int line = ( info == null ) ? 0 : info.getLine();
	if ( line != 0 )
	    msg.append( " na liniji " ).append( line );
	log.info( msg.toString() );
    }

    private enum ObjFoundState {
	ObjFound, ObjAlreadyDeclared, ObjNotFound
    }

    public String printObjNode( Obj obj ) {
	StringBuilder msg = new StringBuilder();

	switch (obj.getKind() ) {
	case Obj.Con:
	    msg.append( "Con " );
	    break;
	case Obj.Var:
	    msg.append( "Var " );
	    break;
	case Obj.Type:
	    msg.append( "Type " );
	    break;
	case Obj.Meth:
	    msg.append( "Meth " );
	    break;
	case Obj.Fld:
	    msg.append( "Fld " );
	    break;
	case Obj.Prog:
	    msg.append( "Prog " );
	    break;
	}

	msg.append( obj.getName() );
	msg.append( ": " );

	switch (obj.getType().getKind() ) {
	case Struct.None:
	    msg.append( "notype" );
	    break;
	case Struct.Int:
	    msg.append( "int" );
	    break;
	case Struct.Char:
	    msg.append( "char" );
	    break;
	case Struct.Array:
	    msg.append( "Arr of " );

	    switch (obj.getType().getElemType().getKind() ) {
	    case Struct.None:
		msg.append( "notype" );
		break;
	    case Struct.Int:
		msg.append( "int" );
		break;
	    case Struct.Char:
		msg.append( "char" );
		break;
	    case Struct.Class:
		msg.append( "Class" );
		break;
	    }
	    break;
	case Struct.Class:
	    msg.append( "Class [" );

	    Collection<Obj> mems = obj.getType().getMembers();
	    for ( Obj objMem : mems ) {
		msg.append( objMem.getName() + ", " );
	    }
	    if ( mems.size() > 0 ) {
		msg.deleteCharAt( msg.length() - 2 );
		msg.deleteCharAt( msg.length() - 1 );
	    }
	    msg.append( "]" );
	    break;
	}

	msg.append( ", " );
	msg.append( obj.getAdr() );
	msg.append( ", " );
	msg.append( obj.getLevel() + " " );

	return msg.toString();
    }

    public void symbolDetection( int lineNum, String symName, Obj symbolObj,
	    ObjFoundState state ) {
	StringBuilder msg = new StringBuilder();
	switch (state ) {
	case ObjFound:
	    msg.append( "Pretraga na " + lineNum + "(" + symName + "),"
		    + " nadjeno " + printObjNode( symbolObj ) );
	    break;
	case ObjAlreadyDeclared:
	    msg.append( "Greska na " + lineNum + ": " + symName
		    + " vec deklarisano" );
	    break;
	case ObjNotFound:
	    msg.append(
		    "Greska na " + lineNum + ": " + symName + " nije nadjeno" );
	    break;
	}
	log.info( msg.toString() );
    }

    // metode za obradu sintaksnog stabla

    // program
    public void visit( ProgramName progName ) {
	progName.obj = Tab.insert( Obj.Prog, progName.getPName(), Tab.noType );
	Tab.openScope();
	report_info( "Registrovan program sa imenom " + progName.getPName(),
		progName );
    }

    public void visit( Program program ) {
	boolean mainError = false;
	Obj mainMeth = Tab.find( "main" );
	mainError = mainError || mainMeth == Tab.noObj;
	mainError = mainError || mainMeth.getLevel() > 0;
	mainError = mainError || mainMeth.getType() != Tab.noType;
	if ( mainError ) {
	    report_error( "U programu nije definisana validna main metoda!",
		    null );
	}
	nVars = Tab.currentScope.getnVars();
	Tab.chainLocalSymbols( program.getProgramName().obj );
	Tab.closeScope();
	report_info( "Zavrsena semanticka analiza!", null );
    }

    // deklarisanje promenljivih
    private Struct currentType = Tab.noType;

    public void visit( Type type ) {
	Obj typeNode = Tab.find( type.getTypeName() );
	if ( typeNode == Tab.noObj ) {
	    report_error( "Nije pronadjen tip " + type.getTypeName()
		    + " u tabeli simbola! ", null );
	    type.struct = Tab.noType;
	} else {
	    if ( Obj.Type == typeNode.getKind() ) {
		type.struct = typeNode.getType();
	    } else {
		report_error( "Greska: Ime " + type.getTypeName()
			+ " ne predstavlja tip!", type );
		type.struct = Tab.noType;
	    }
	}
	currentType = type.struct;
    }

    private void declareVar( String name, Struct struct, SyntaxNode node ) {
	Obj typeNode = Tab.find( name );
	int currLevel = currentMethod != null ? 1 : 0;
	currLevel += currentRecord != null ? 1 : 0;

	if ( typeNode != Tab.noObj && currLevel == typeNode.getLevel() ) {
	    symbolDetection( node.getLine(), name, typeNode,
		    ObjFoundState.ObjAlreadyDeclared );
	    return;
	}
	report_info( "Deklarisana promenljiva " + name, node );

	if ( currLevel == 0 ) {
	    numOfGlobals++ ;
	    if ( struct.getKind() == Struct.Array ) {
		numOfGlobalArrs++ ;
	    }
	} else if ( currentMethod != null
		&& currentMethod.getName().equals( "main" ) ) {
	    numOfMainLocals++ ;
	}

	int elemType = currentRecord == null ? Obj.Var : Obj.Fld;
	Tab.insert( elemType, name, struct );
    }

    public void visit( Array array ) {
	array.struct = new Struct( Struct.Array, array.getArr().struct );
    }

    public void visit( NoArrDecl noArrDecl ) {
	noArrDecl.struct = currentType;
    }

    public void visit( VarDeclarationSingle varDecl ) {
	declareVar( varDecl.getVarName(), varDecl.getArr().struct, varDecl );
    }

    public void visit( VarParamListStart varDeclStart ) {
	declareVar( varDeclStart.getVarName(), varDeclStart.getArr().struct,
		varDeclStart );
    }

    public void visit( VarParamListClass varParamList ) {
	declareVar( varParamList.getVarName(), varParamList.getArr().struct,
		varParamList );
    }

    public void visit( VarDeclarationMany varDeclarationMany ) {
	declareVar( varDeclarationMany.getVarName(),
		varDeclarationMany.getArr().struct, varDeclarationMany );
    }

    // deklarisanje konstanti
    private void declareConst( String name, ConstVal cVal ) {
	Obj con = Tab.insert( Obj.Con, name, cVal.struct );
	con.setLevel( 0 );

	if ( cVal instanceof ConstBool ) {
	    con.setAdr( ( (ConstBool) cVal ).getB() ? 1 : 0 );
	} else if ( cVal instanceof ConstNum ) {
	    con.setAdr( ( (ConstNum) cVal ).getNum() );
	} else {
	    con.setAdr( ( (ConstChar) cVal ).getCh() );
	}

	if ( con.getLevel() == 0 ) {
	    numOfConsts++ ;
	}
    }

    public void visit( ConstListClass constListClass ) {
	if ( constListClass.getConstVal().struct != currentType ) {
	    report_error( "Semanticka greska na liniji "
		    + constListClass.getLine() + ": konstanta "
		    + constListClass.getVarName() + " nije odgovarajuceg tipa!",
		    null );
	} else {
	    declareConst( constListClass.getVarName(),
		    constListClass.getConstVal() );
	}
    }

    public void visit( SingleConst singleConst ) {
	if ( singleConst.getConstVal().struct != currentType ) {
	    report_error( "Semanticka greska na liniji " + singleConst.getLine()
		    + ": konstanta " + singleConst.getVarName()
		    + " nije odgovarajuceg tipa!", null );
	} else {
	    declareConst( singleConst.getVarName(), singleConst.getConstVal() );
	}
    }

    public void visit( ConstBool constBool ) {
	constBool.struct = Tab.find( "bool" ).getType();
    }

    public void visit( ConstNum constNum ) {
	constNum.struct = Tab.intType;
    }

    public void visit( ConstChar constChar ) {
	constChar.struct = Tab.charType;
    }

    // deklarisanje metoda
    public void visit( MethodReturnType methodRetType ) {
	methodRetType.struct = methodRetType.getType().struct;
    }

    public void visit( VoidClass voidClass ) {
	voidClass.struct = Tab.noType;
    }

    public void visit( MethodTypeName methodTypeName ) {
	currentMethod = Tab.insert( Obj.Meth, methodTypeName.getMethName(),
		methodTypeName.getMethodRetType().struct );
	methodTypeName.obj = currentMethod;
	Tab.openScope();
	report_info( "Obradjuje se funkcija " + methodTypeName.getMethName(),
		methodTypeName );
    }

    public void visit( FormalParamDecl formalParamDecl ) {
	declareVar( formalParamDecl.getName(), formalParamDecl.getArr().struct,
		formalParamDecl );
	numberOfParams++ ;
    }

    public void visit( MethodDefinitionStart methodDefStart ) {
	Tab.chainLocalSymbols( currentMethod );
	currentMethod.setLevel( numberOfParams );
    }

    public void visit( MethodDeclaration methodDecl ) {
	Tab.closeScope();
	if ( !returnFound && currentMethod.getType() != Tab.noType ) {
	    report_error( "Semanticka greska na liniji " + methodDecl.getLine()
		    + ": funkcija " + currentMethod.getName()
		    + " nema return iskaz!", null );
	}
	report_info( "Zavrsena obrada funkcije " + currentMethod.getName(),
		methodDecl );

	numOfMethods++ ;
	returnFound = false;
	currentMethod = null;
	numberOfParams = 0;
    }

    public void visit( ReturnExpr returnExpr ) {
	returnFound = true;
	Struct currMethType = currentMethod.getType();
	if ( !currMethType.compatibleWith( returnExpr.getExpr().struct ) ) {
	    report_error( "Greska na liniji " + returnExpr.getLine() + " : "
		    + "tip izraza u return naredbi ne slaze se sa tipom povratne vrednosti funkcije "
		    + currentMethod.getName(), null );
	}
    }

    public void visit( ReturnNoExpr returnNoExpr ) {
	returnFound = true;
	Struct currMethType = currentMethod.getType();
	if ( !currMethType.compatibleWith( Tab.noType ) ) {
	    report_error( "Greska na liniji " + returnNoExpr.getLine() + " : "
		    + "tip izraza u return naredbi ne slaze se sa tipom povratne vrednosti funkcije "
		    + currentMethod.getName(), null );
	}
    }

    // designator
    private Obj currentRecordReferencing = null;

    private Struct checkArrayNesting( Obj arr, int arrRefCount, String name,
	    int line ) {
	Struct objStruct = arr.getType();
	for ( int i = 0; i < arrRefCount; i++ ) {
	    if ( objStruct.getKind() != Struct.Array ) {
		report_error( "Greska na liniji " + line + " : promenljiva "
			+ name + " nije niz! ", null );
		return Tab.noType;
	    }
	    objStruct = objStruct.getElemType();
	}
	return objStruct;
    }

    public void visit( DesName desName ) {
	Obj obj;
	if ( currentRecordReferencing == null ) {
	    obj = Tab.find( desName.getName() );
	} else {
	    obj = currentRecordReferencing.getType().getMembersTable()
		    .searchKey( desName.getName() );
	    if ( obj == null ) {
		obj = Tab.noObj;
	    }
	}

	symbolDetection( desName.getLine(), desName.getName(), obj,
		obj == Tab.noObj ? ObjFoundState.ObjNotFound
			: ObjFoundState.ObjFound );

	if ( obj == null || obj == Tab.noObj ) {
	    desName.obj = Tab.noObj;
	    return;
	}

	if ( obj.getKind() == Obj.Meth ) {
	    currentMethodCall
		    .push( new Obj( Obj.Meth, obj.getName(), obj.getType() ) );
	    report_info( "Dodata metoda za poziv " + desName.getName(),
		    desName.getParent() );
	    Tab.openScope();
	}

	desName.obj = obj;
	currentRecordReferencing = null;
    }

    public void visit( ReferencingDot refDot ) {
	currentRecordReferencing = ( (DesignatorClass) refDot.getParent() )
		.getDesignator().obj;
    }

    public void visit( SimpleDesignator simpleDesignator ) {
	int numRef = simpleDesignator.getArrRef().integer;
	Obj obj = simpleDesignator.getDesName().obj;

	if ( numRef > 0 ) {
	    String name = simpleDesignator.getDesName().getName();
	    simpleDesignator.obj = new Obj( Obj.Elem, name,
		    checkArrayNesting( obj, numRef, name,
			    simpleDesignator.getLine() ),
		    obj.getAdr(), obj.getLevel() );
	    simpleDesignator.obj.setFpPos( numRef );
	} else {
	    simpleDesignator.obj = obj;
	}
    }

    public void visit( SimpleDesignatorClass simpleDesignator ) {
	simpleDesignator.obj = simpleDesignator.getSimpleDesignator().obj;
    }

    public void visit( DesignatorClass designator ) {
	if ( designator.getDesignator().obj.getType()
		.getKind() != Struct.Class ) {
	    report_error( "Greska na liniji " + designator.getLine() + " : "
		    + " samo se recordi mogu dereferencirat!", null );
	    designator.obj = Tab.noObj;
	    return;
	}

	SymbolDataStructure currentLocals = designator.getDesignator().obj
		.getType().getMembersTable();
	if ( currentLocals == null || currentLocals.numSymbols() == 0 ) {
	    report_error( "Greska na liniji " + designator.getLine() + " : "
		    + " samo se recordi mogu dereferencirat!", null );
	    designator.obj = Tab.noObj;
	    return;
	}

	Obj currentIdent = currentLocals
		.searchKey( designator.getSimpleDesignator().obj.getName() );
	if ( currentIdent == null ) {
	    report_error( "Greska na liniji " + designator.getLine() + " : "
		    + designator.getSimpleDesignator().obj.getName()
		    + " ne postoji u trenutnom opsegu!", null );
	    designator.obj = Tab.noObj;
	    return;
	}

	int numRef = designator.getSimpleDesignator().getArrRef().integer;
	if ( numRef > 0 ) {
	    designator.obj = new Obj( Obj.Elem,
		    designator.getSimpleDesignator().obj.getName(),
		    checkArrayNesting( currentIdent, numRef,
			    designator.getSimpleDesignator().obj.getName(),
			    designator.getLine() ),
		    currentIdent.getAdr(), currentIdent.getLevel() );
	} else {
	    designator.obj = currentIdent;
	}
    }

    public void visit( ArrayRefList arrayRefList ) {
	arrayRefList.integer = arrayRefList.getArrRef().integer + 1;
	if ( arrayRefList.getExpr().struct != Tab.intType ) {
	    report_error( "Greska na liniji " + arrayRefList.getLine()
		    + " : prilikom dereferenciranja niza, izraz mora biti tipa int!",
		    null );
	    arrayRefList.integer = 0;
	    return;
	}
    }

    public void visit( NoArrayRef noArrayRef ) {
	noArrayRef.integer = 0;
    }

    public void visit( ArrayRefListAlloc arrayRefList ) {
	arrayRefList.integer = arrayRefList.getArrRefAlloc().integer + 1;
	if ( arrayRefList.getExpr().struct != Tab.intType ) {
	    report_error( "Greska na liniji " + arrayRefList.getLine()
		    + " : prilikom alokacije niza, izraz mora biti tipa int!",
		    null );
	    arrayRefList.integer = 0;
	    return;
	}
    }

    public void visit( NoArrayRefAlloc noArrayRef ) {
	noArrayRef.integer = 0;
    }

    // factors
    public void visit( ConstNumFactor constNumFactor ) {
	constNumFactor.struct = Tab.intType;
    }

    public void visit( ConstCharFactor constCharFactor ) {
	constCharFactor.struct = Tab.charType;
    }

    public void visit( ConstBoolFactor constBoolFactor ) {
	constBoolFactor.struct = Tab.find( "bool" ).getType();
    }

    public void visit( AllocationExpr allocationExpr ) {
	Struct st = allocationExpr.getType().struct;

	for ( int i = allocationExpr.getArrRefAlloc().integer; i > 0; i-- ) {
	    st = new Struct( Struct.Array, st );
	}
	allocationExpr.struct = st;
    }

    public void visit( ParenExpression parenExpression ) {
	parenExpression.struct = parenExpression.getExpr().struct;
    }

    // terms
    public void visit( TermMulopList termMulopList ) {
	if ( termMulopList.getFactor().struct == Tab.intType
		&& termMulopList.getFactor().struct == Tab.intType ) {
	    termMulopList.struct = termMulopList.getFactor().struct;
	} else {
	    report_error(
		    "Greska na liniji " + termMulopList.getLine() + " : "
			    + "oba cinioca moraju biti tipa int!",
		    termMulopList );
	    termMulopList.struct = Tab.noType;
	}
    }

    public void visit( TermNoMulopList termNoMulopList ) {
	termNoMulopList.struct = termNoMulopList.getFactor().struct;
    }

    public void visit( MulopFactor mulopFactor ) {
	if ( mulopFactor.getFactor().struct == Tab.intType
		&& mulopFactor.getMulFactor().struct == Tab.intType ) {
	    mulopFactor.struct = mulopFactor.getFactor().struct;
	} else {
	    report_error(
		    "Greska na liniji " + mulopFactor.getLine() + " : "
			    + "oba cinioca moraju biti tipa int!",
		    mulopFactor );
	    mulopFactor.struct = Tab.noType;
	}
    }

    public void visit( MulopFactorSingle mulopFactorSingle ) {
	mulopFactorSingle.struct = mulopFactorSingle.getFactor().struct;
    }

    // expr
    public void visit( MinusTermExpr minusTermExpr ) {
	if ( minusTermExpr.getTerm().struct != Tab.intType ) {
	    report_error( "Greska na liniji " + minusTermExpr.getLine() + " : "
		    + "term mora biti tipa int!", null );
	    minusTermExpr.struct = Tab.noType;
	} else {
	    minusTermExpr.struct = minusTermExpr.getTerm().struct;
	}
    }

    public void visit( TermExpression termExpression ) {
	termExpression.struct = termExpression.getTerm().struct;
    }

    public void visit( AddExpressionList addExpressionList ) {
	// compatibleWith(
	if ( addExpressionList.getAddExprList().struct == Tab.intType
		&& addExpressionList.getTerm().struct == Tab.intType ) {
	    addExpressionList.struct = addExpressionList.getTerm().struct;
	} else {
	    report_error( "Greska na liniji " + addExpressionList.getLine()
		    + " : " + "Expr i term moraju biti tipa int!", null );
	}
    }

    public void visit( SingleAddExpressionList singleAddExpressionList ) {
	singleAddExpressionList.struct = singleAddExpressionList
		.getTerm().struct;
    }

    public void visit( ExpressionAddList expressionAddList ) {
	if ( expressionAddList.getAddExprList().struct == Tab.intType
		&& expressionAddList.getTermExpr().struct == Tab.intType ) {
	    expressionAddList.struct = expressionAddList
		    .getAddExprList().struct;
	} else {
	    report_error( "Greska na liniji " + expressionAddList.getLine()
		    + " : " + "Expr i term moraju biti tipa int!", null );
	}
    }

    public void visit( ExpressionNoAddList expressionNoAddList ) {
	expressionNoAddList.struct = expressionNoAddList.getTermExpr().struct;
    }

    // procedure calls
    private boolean checkTypeCompatibility( Struct t1, Struct t2 ) {
	Struct tmp1 = t1;
	Struct tmp2 = t2;
	if ( tmp1.getKind() == Struct.Array ) {
	    while ( tmp1.getKind() == Struct.Array
		    && tmp2.getKind() == Struct.Array ) {
		tmp1 = tmp1.getElemType();
		tmp2 = tmp2.getElemType();
	    }
	}
	return tmp1.compatibleWith( tmp2 );
    }

    private void addParamToObj( Struct str ) {
	nVars = currentMethodCall.peek().getLevel();
	nVars += nVars < 0 ? 1 : 0;
	Tab.insert( Obj.Var, "param" + nVars, str );
	currentMethodCall.peek().setLevel( nVars + 1 );
    }

    public void visit( ActParsClass actPars ) {
	addParamToObj( actPars.getExpr().struct );
    }

    public void visit( SingleActPar singleActPar ) {
	addParamToObj( singleActPar.getExpr().struct );
    }

    private void checkFormalActualParams( Obj formalMethodObj,
	    Obj actualMethodObj, int lineNum ) {
	if ( currentMethod != null
		&& currentMethod.getName().equals( "main" ) ) {
	    numOfMainFuncCalls++ ;
	}
	if ( formalMethodObj.getLevel() == 0
		&& actualMethodObj.getLevel() == -1 ) {
	    // empty function call
	    return;
	}
	if ( formalMethodObj.getLevel() != actualMethodObj.getLevel() ) {
	    report_error( "Greska na liniji " + lineNum
		    + " : pogresan broj parametara!", null );
	    return;
	}
	try {
	    Obj[] formalParams = formalMethodObj.getLocalSymbols().toArray(
		    new Obj[formalMethodObj.getLocalSymbols().size()] );
	    Obj[] actualParams = actualMethodObj.getLocalSymbols().toArray(
		    new Obj[actualMethodObj.getLocalSymbols().size()] );
	    for ( int i = 0; i < formalMethodObj.getLevel(); i++ ) {
		if ( !checkTypeCompatibility( formalParams[i].getType(),
			actualParams[i].getType() ) ) {
		    report_error( "Greska na liniji " + lineNum
			    + " : parametar " + i + " nije potrebnog tipa!",
			    null );
		}
	    }
	} catch ( NullPointerException e ) {
	    report_error(
		    "Greska na liniji " + lineNum + " : nesto je puklo :)!",
		    null );
	}
    }

    public void visit( CallProc callProc ) {
	checkDesignatorMethod( callProc.getDesignator().obj,
		callProc.getLine() );

	Obj obj = currentMethodCall.pop();
	Tab.chainLocalSymbols( obj );
	Tab.closeScope();

	checkFormalActualParams( Tab.find( obj.getName() ), obj,
		callProc.getLine() );
    }

    public void visit( CallProcEmpty callProcEmpty ) {
	checkDesignatorMethod( callProcEmpty.getDesignator().obj,
		callProcEmpty.getLine() );

	if ( currentMethodCall.size() > 0 ) {
	    Obj obj = currentMethodCall.pop();
	    Tab.chainLocalSymbols( obj );
	    Tab.closeScope();

	    checkFormalActualParams( Tab.find( obj.getName() ), obj,
		    callProcEmpty.getLine() );
	} else {
	    report_error( "Greska na liniji " + callProcEmpty.getLine()
		    + " : stek poziva funkcija je pukao!", null );
	}
    }

    public void visit( DesignatorFactorNoParams designatorFactorNoParams ) {
	designatorFactorNoParams.struct = designatorFactorNoParams
		.getDesignator().obj.getType();
    }

    public void visit( DesignatorFactorParams designatorFactorParams ) {
	designatorFactorParams.struct = designatorFactorParams
		.getDesignator().obj.getType();

	Obj obj = currentMethodCall.pop();
	Tab.chainLocalSymbols( obj );
	Tab.closeScope();

	checkFormalActualParams( Tab.find( obj.getName() ), obj,
		designatorFactorParams.getLine() );
    }

    // conditions
    private Struct checkBothSidesBoolI( Struct lExpr, Struct rExpr,
	    int numLine ) {
	if ( lExpr == null || lExpr.getKind() != Struct.Bool
		&& rExpr.getKind() != Struct.Bool ) {
	    report_error( "Greska na liniji " + numLine
		    + " : oba izraza moraju biti tipa bool!", null );
	    return Tab.noType;
	}
	return lExpr;
    }

    public void visit( ConditionClass conditionClass ) {
	conditionClass.struct = checkBothSidesBoolI(
		conditionClass.getCondition().struct,
		conditionClass.getCondTerm().struct, conditionClass.getLine() );
    }

    public void visit( BasicCondTerm basicCondTerm ) {
	basicCondTerm.struct = basicCondTerm.getCondTerm().struct;
    }

    public void visit( ConditionTerm conditionTerm ) {
	conditionTerm.struct = checkBothSidesBoolI(
		conditionTerm.getCondTerm().struct,
		conditionTerm.getCondFact().struct, conditionTerm.getLine() );
    }

    public void visit( BasicCondFact basicCondFact ) {
	basicCondFact.struct = basicCondFact.getCondFact().struct;
    }

    public void visit( CondFactNoRelation condFactNoRelation ) {
	condFactNoRelation.struct = condFactNoRelation.getExpr().struct;
    }

    public void visit( CondFactRelation condFactRelation ) {
	Struct lExpr = condFactRelation.getExpr().struct;
	Struct rExpr = condFactRelation.getExpr1().struct;
	if ( !lExpr.compatibleWith( rExpr ) ) {
	    report_error( "Greska na liniji " + condFactRelation.getLine()
		    + " : tipovi nisu kompatibilni!", null );
	    return;
	}
	if ( lExpr.getKind() == Struct.Array && ( !( condFactRelation
		.getRelop() instanceof RelEqual )
		&& !( condFactRelation.getRelop() instanceof NotEqual ) ) ) {
	    report_error( "Greska na liniji " + condFactRelation.getLine()
		    + " : nizovi se mogu uporedjivati samo na jednakost i nejednakost!",
		    null );
	    return;
	}
	condFactRelation.struct = new Struct( Struct.Bool );
    }

    // designatorStatement
    private void checkDesignatorAssignable( Obj obj, int lineNum ) {
	if ( obj.getKind() != Obj.Var && obj.getKind() != Obj.Elem ) {
	    report_error( "Greska na liniji " + lineNum
		    + " : vrednost se moze menjati samo promenljivama i elementima nizova!",
		    null );
	    return;
	}
    }

    private boolean checkDesignatorType( Struct str, int lineNum, int type,
	    boolean writeError ) {
	String s = "int";
	switch (type ) {
	case Struct.Char:
	    s = "char";
	    break;
	case Struct.Bool:
	    s = "bool";
	    break;
	}
	boolean b = str.getKind() == type;
	if ( writeError && !b ) {
	    report_error( "Greska na liniji " + lineNum
		    + " : ova operacija se moze izvrsiti samo za tip " + s
		    + "!", null );
	}
	return b;
    }

    private void checkDesignatorPrimitiveType( Struct str, int lineNum ) {
	boolean b = checkDesignatorType( str, lineNum, Struct.Int, false );
	if ( !b ) {
	    b = checkDesignatorType( str, lineNum, Struct.Char, false );
	}
	if ( !b ) {
	    b = checkDesignatorType( str, lineNum, Struct.Bool, false );
	}
	if ( !b ) {
	    report_error( "Greska na liniji " + lineNum
		    + " : ova operacija zahteva samo tipove int, char i bool!",
		    null );
	}
    }

    private void checkDesignatorMethod( Obj obj, int lineNum ) {
	if ( obj.getKind() != Obj.Meth ) {
	    report_error(
		    "Greska na liniji " + lineNum + " : ovo nije funkcija!",
		    null );
	}
    }

    public void visit( AssignExprClass assignExprClass ) {
	assignExprClass.struct = assignExprClass.getExpr().struct;
    }

    public void visit( Assign assign ) {
	Struct st = assign.getDesignator().obj.getType();
	Struct as = assign.getAssignExpr().struct;
	if ( as != null && !checkTypeCompatibility( st, as ) ) {
	    report_error(
		    "Greska na liniji " + assign.getLine()
			    + " : ovi tipovi nisu kompatibilni za dodelu!",
		    null );
	    return;
	}
    }

    public void visit( PlusPlus plusPlus ) {
	checkDesignatorAssignable( plusPlus.getDesignator().obj,
		plusPlus.getLine() );
	checkDesignatorType( plusPlus.getDesignator().obj.getType(),
		plusPlus.getLine(), Struct.Int, true );
    }

    public void visit( MinusMinus minusMinus ) {
	checkDesignatorAssignable( minusMinus.getDesignator().obj,
		minusMinus.getLine() );
	checkDesignatorType( minusMinus.getDesignator().obj.getType(),
		minusMinus.getLine(), Struct.Int, true );
    }

    // statements
    /*
     * public void visit(LabeledSingleStatement labeledSingleStatement){
     * Tab.insert(Obj.Con, labeledSingleStatement.getLabel().getName(), new
     * Struct(Struct.None)); }
     * 
     * public void visit(Goto gotoStatement){ Obj label =
     * Tab.find(gotoStatement.getLabel().getName()); if( label == Tab.noObj ){
     * report_error("Nije pronadjena labela " + gotoStatement.getLabel() +
     * " u tabeli simbola! ", null); return; } /* if( label.getLevel() !=
     * Tab.getCurrentLevel() ){ report_error("Pronadjena labela " +
     * gotoStatement.getLabel() + " nije u trenutnom opsegu! ", null); return; }
     * / }
     */

    public void visit( DoStart doStart ) {
	doWhileCounter++ ;
	report_info( "Zapocinje se obrada DO-WHILE petlje ", doStart );
    }

    public void visit( Do doClass ) {
	if ( --doWhileCounter < 0 ) {
	    report_error( "Pogresno ugnjezdavanje do-while petlji na liniji"
		    + doClass.getLine(), null );
	} else {
	    report_info( "Zatvara se DO-WHILE petlja ", doClass );
	}
	if ( !checkDesignatorType( doClass.getCondition().struct,
		doClass.getLine(), Struct.Bool, false ) ) {
	    report_error(
		    "Greska na liniji " + doClass.getLine()
			    + " : uslov u DO-WHILE petlji mora biti tipa bool!",
		    null );
	}
    }

    public void visit( Break breakClass ) {
	if ( doWhileCounter <= 0 ) {
	    report_error(
		    "Break naredba mora stajati unutar do-while petlje! Greska ",
		    breakClass );
	}
    }

    public void visit( Continue continueClass ) {
	if ( doWhileCounter <= 0 ) {
	    report_error(
		    "Continue naredba mora stajati unutar do-while petlje! Greska ",
		    continueClass );
	}
    }

    public void visit( Read read ) {
	checkDesignatorAssignable( read.getDesignator().obj, read.getLine() );
	checkDesignatorPrimitiveType( read.getDesignator().obj.getType(),
		read.getLine() );
    }

    public void visit( PrintConst printConst ) {
	checkDesignatorPrimitiveType( printConst.getExpr().struct,
		printConst.getLine() );
    }

    public void visit( PrintNoConst printNoConst ) {
	checkDesignatorPrimitiveType( printNoConst.getExpr().struct,
		printNoConst.getLine() );
    }

    private void checkIfCondition( Struct cond, int lineNum ) {
	if ( cond == null
		|| !checkDesignatorType( cond, lineNum, Struct.Bool, false ) ) {
	    report_error(
		    "Greska na liniji " + lineNum
			    + " : uslov u if izrazu mora biti tipa bool!",
		    null );
	}
    }

    public void visit( IfConditionClass ifConditionClass ) {
	ifConditionClass.struct = ifConditionClass.getCondition().struct;
    }

    public void visit( IfElseMatched ifElseMatched ) {
	checkIfCondition( ifElseMatched.getIfCondition().struct,
		ifElseMatched.getLine() );
    }

    public void visit( IfUnmatched ifUnmatched ) {
	checkIfCondition( ifUnmatched.getIfCondition().struct,
		ifUnmatched.getLine() );
    }

    public void visit( IfElseUnmatched ifElseUnmatched ) {
	checkIfCondition( ifElseUnmatched.getIfCondition().struct,
		ifElseUnmatched.getLine() );
    }

    // record
    public void visit( RecordName recordName ) {
	currentRecord = Tab.insert( Obj.Type, recordName.getRName(),
		new Struct( Struct.Class ) );
	recordName.obj = currentRecord;
	Tab.openScope();
	Tab.insert( Obj.Var, "this", currentRecord.getType() );
	report_info( "Obradjuje se record " + recordName.getRName(),
		recordName );
	numOfRecords++ ;
    }

    private void declareRecord( SyntaxNode node ) {
	Struct st = currentRecord.getType();
	Tab.chainLocalSymbols( st );
	Tab.closeScope();

	report_info( "Zavrsena obrada recorda " + currentRecord.getName(),
		node );

	currentRecord = null;
    }

    public void visit( RecordDeclarationsList recordDeclarationsList ) {
	declareRecord( recordDeclarationsList );
    }

    public void visit( RecordNoDeclarationsList recordNoDeclarationsList ) {
	declareRecord( recordNoDeclarationsList );
    }

    // statements
    public void visit( SingleStmt singleStmt ) {
	if ( currentMethod != null
		&& currentMethod.getName().equals( "main" ) ) {
	    numOfMainStatements++ ;
	}
    }

    public void visit( StatementListClass statementListClass ) {
	if ( currentMethod != null
		&& currentMethod.getName().equals( "main" ) ) {
	    numOfMainStatements++ ;
	}
    }

    public boolean passed() {
	return !errorDetected;
    }

}
