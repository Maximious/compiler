// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class ClassExtend extends ClassDecl {

    private ClassName ClassName;
    private Type Type;
    private ClassDeclSec ClassDeclSec;

    public ClassExtend (ClassName ClassName, Type Type, ClassDeclSec ClassDeclSec) {
        this.ClassName=ClassName;
        if(ClassName!=null) ClassName.setParent(this);
        this.Type=Type;
        if(Type!=null) Type.setParent(this);
        this.ClassDeclSec=ClassDeclSec;
        if(ClassDeclSec!=null) ClassDeclSec.setParent(this);
    }

    public ClassName getClassName() {
        return ClassName;
    }

    public void setClassName(ClassName ClassName) {
        this.ClassName=ClassName;
    }

    public Type getType() {
        return Type;
    }

    public void setType(Type Type) {
        this.Type=Type;
    }

    public ClassDeclSec getClassDeclSec() {
        return ClassDeclSec;
    }

    public void setClassDeclSec(ClassDeclSec ClassDeclSec) {
        this.ClassDeclSec=ClassDeclSec;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassName!=null) ClassName.accept(visitor);
        if(Type!=null) Type.accept(visitor);
        if(ClassDeclSec!=null) ClassDeclSec.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassName!=null) ClassName.traverseTopDown(visitor);
        if(Type!=null) Type.traverseTopDown(visitor);
        if(ClassDeclSec!=null) ClassDeclSec.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassName!=null) ClassName.traverseBottomUp(visitor);
        if(Type!=null) Type.traverseBottomUp(visitor);
        if(ClassDeclSec!=null) ClassDeclSec.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassExtend(\n");

        if(ClassName!=null)
            buffer.append(ClassName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Type!=null)
            buffer.append(Type.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ClassDeclSec!=null)
            buffer.append(ClassDeclSec.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassExtend]");
        return buffer.toString();
    }
}
