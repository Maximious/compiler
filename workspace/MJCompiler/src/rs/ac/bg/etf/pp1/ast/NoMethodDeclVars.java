// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class NoMethodDeclVars extends MethodDeclSec {

    private MethodDefStart MethodDefStart;
    private MethodDef MethodDef;

    public NoMethodDeclVars (MethodDefStart MethodDefStart, MethodDef MethodDef) {
        this.MethodDefStart=MethodDefStart;
        if(MethodDefStart!=null) MethodDefStart.setParent(this);
        this.MethodDef=MethodDef;
        if(MethodDef!=null) MethodDef.setParent(this);
    }

    public MethodDefStart getMethodDefStart() {
        return MethodDefStart;
    }

    public void setMethodDefStart(MethodDefStart MethodDefStart) {
        this.MethodDefStart=MethodDefStart;
    }

    public MethodDef getMethodDef() {
        return MethodDef;
    }

    public void setMethodDef(MethodDef MethodDef) {
        this.MethodDef=MethodDef;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodDefStart!=null) MethodDefStart.accept(visitor);
        if(MethodDef!=null) MethodDef.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodDefStart!=null) MethodDefStart.traverseTopDown(visitor);
        if(MethodDef!=null) MethodDef.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodDefStart!=null) MethodDefStart.traverseBottomUp(visitor);
        if(MethodDef!=null) MethodDef.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("NoMethodDeclVars(\n");

        if(MethodDefStart!=null)
            buffer.append(MethodDefStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDef!=null)
            buffer.append(MethodDef.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [NoMethodDeclVars]");
        return buffer.toString();
    }
}
