// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class PossRecDeclaration extends PossDecl {

    private RecDecl RecDecl;

    public PossRecDeclaration (RecDecl RecDecl) {
        this.RecDecl=RecDecl;
        if(RecDecl!=null) RecDecl.setParent(this);
    }

    public RecDecl getRecDecl() {
        return RecDecl;
    }

    public void setRecDecl(RecDecl RecDecl) {
        this.RecDecl=RecDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(RecDecl!=null) RecDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(RecDecl!=null) RecDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(RecDecl!=null) RecDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("PossRecDeclaration(\n");

        if(RecDecl!=null)
            buffer.append(RecDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [PossRecDeclaration]");
        return buffer.toString();
    }
}
