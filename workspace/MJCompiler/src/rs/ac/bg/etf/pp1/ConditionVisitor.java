package rs.ac.bg.etf.pp1;

import java.util.HashMap;

import rs.ac.bg.etf.pp1.ConditionVisitor.ConditionJmpAdr;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.mj.runtime.Code;

public class ConditionVisitor extends VisitorAdaptor {

    public static class ConditionJmpAdr {
	public int conditionStartAdr;
	public int jmpTrueAdr;
	public int jmpFalseAdr;

	public ConditionJmpAdr( int startAdr, int jmpT, int jmpF ) {
	    conditionStartAdr = startAdr;
	    jmpTrueAdr = jmpT;
	    jmpFalseAdr = jmpF;
	}
    }

    private HashMap<SyntaxNode, ConditionJmpAdr> conditionJmps = 
	    new HashMap<SyntaxNode, ConditionJmpAdr>();

    public void resetConditionJmps() {
	conditionJmps = new HashMap<SyntaxNode, ConditionJmpAdr>();
    }

    public void addCondition( SyntaxNode node, 
	    ConditionJmpAdr conditionJmpAdr ) {
	conditionJmps.put( node, conditionJmpAdr );
    }

    public ConditionJmpAdr getCondition( SyntaxNode node ) {
	return conditionJmps.get( node );
    }

    private void printCurrentState( ConditionJmpAdr conditionJmpAdr,
	    SyntaxNode node ) {
	System.out.println( conditionJmpAdr.jmpTrueAdr + " " + 
	    conditionJmpAdr.jmpFalseAdr + node.getClass() );
    }

    // visit methods
    public void visit( IfConditionClass conditionEnd ) {
	ConditionJmpAdr conditionJmpAdrPar = 
		conditionJmps.get( conditionEnd.getParent() );
	ConditionJmpAdr conditionJmpAdrCur = 
		conditionJmps.get( conditionEnd );

	conditionJmpAdrCur.jmpTrueAdr = conditionJmpAdrPar.jmpTrueAdr;
	conditionJmpAdrCur.jmpFalseAdr = conditionJmpAdrPar.jmpFalseAdr;
    }

    public void visit( ConditionClass conditionClass ) {
	ConditionJmpAdr conditionJmpAdrPar = 
		conditionJmps.get( conditionClass.getParent() );
	ConditionJmpAdr conditionJmpAdrCur = 
		conditionJmps.get( conditionClass );

	conditionJmpAdrCur.jmpTrueAdr = conditionJmpAdrPar.jmpTrueAdr;
	conditionJmpAdrCur.jmpFalseAdr = conditionJmpAdrPar.jmpFalseAdr;

	if ( conditionClass.getParent() instanceof ConditionClass ) {
	    ConditionClass parent = 
		    (ConditionClass) conditionClass.getParent();
	    ConditionJmpAdr conditionJmpAdrNext = 
		    conditionJmps.get( parent.getCondTerm() );
	    conditionJmpAdrCur.jmpFalseAdr = 
		    conditionJmpAdrNext.conditionStartAdr;
	}

	// printCurrentState( conditionJmpAdrCur, conditionClass );
    }

    public void visit( BasicCondTerm basicCondTerm ) {
	ConditionJmpAdr conditionJmpAdrPar = conditionJmps.get( basicCondTerm.getParent() );
	ConditionJmpAdr conditionJmpAdrCur = conditionJmps.get( basicCondTerm );

	conditionJmpAdrCur.jmpTrueAdr = conditionJmpAdrPar.jmpTrueAdr;
	conditionJmpAdrCur.jmpFalseAdr = conditionJmpAdrPar.jmpFalseAdr;
	if ( basicCondTerm.getParent() instanceof ConditionClass ) {
	    ConditionClass parent = (ConditionClass) basicCondTerm.getParent();
	    ConditionJmpAdr conditionJmpAdrNext = conditionJmps.get( parent.getCondTerm() );
	    conditionJmpAdrCur.jmpFalseAdr = conditionJmpAdrNext.conditionStartAdr;
	}

	// printCurrentState( conditionJmpAdrCur, basicCondTerm );
    }

    public void visit( ConditionTerm conditionTerm ) {
	ConditionJmpAdr conditionJmpAdrPar = conditionJmps.get( conditionTerm.getParent() );
	ConditionJmpAdr conditionJmpAdrCur = conditionJmps.get( conditionTerm );

	conditionJmpAdrCur.jmpTrueAdr = conditionJmpAdrPar.jmpTrueAdr;
	conditionJmpAdrCur.jmpFalseAdr = conditionJmpAdrPar.jmpFalseAdr;
	if ( conditionTerm.getParent() instanceof ConditionTerm ) {
	    ConditionTerm parent = (ConditionTerm) conditionTerm.getParent();
	    ConditionJmpAdr conditionJmpAdrNext = conditionJmps.get( parent.getCondFact() );
	    conditionJmpAdrCur.jmpTrueAdr = conditionJmpAdrNext.conditionStartAdr;
	}

	// printCurrentState( conditionJmpAdrCur, conditionTerm );
    }

    public void visit( BasicCondFact basicCondFact ) {
	ConditionJmpAdr conditionJmpAdrPar = conditionJmps.get( basicCondFact.getParent() );
	ConditionJmpAdr conditionJmpAdrCur = conditionJmps.get( basicCondFact );

	conditionJmpAdrCur.jmpTrueAdr = conditionJmpAdrPar.jmpTrueAdr;
	conditionJmpAdrCur.jmpFalseAdr = conditionJmpAdrPar.jmpFalseAdr;
	if ( basicCondFact.getParent() instanceof ConditionTerm ) {
	    ConditionTerm parent = (ConditionTerm) basicCondFact.getParent();
	    ConditionJmpAdr conditionJmpAdrNext = conditionJmps.get( parent.getCondFact() );
	    conditionJmpAdrCur.jmpTrueAdr = conditionJmpAdrNext.conditionStartAdr;
	}

	// printCurrentState( conditionJmpAdrCur, basicCondFact );
    }

    private void fixCondFact( ConditionJmpAdr conditionJmpAdrParent, ConditionJmpAdr conditionJmpAdrChild ) {
	if ( conditionJmpAdrParent.jmpTrueAdr != 0 ) {
	    Code.put2( conditionJmpAdrChild.jmpTrueAdr,
		    conditionJmpAdrParent.jmpTrueAdr - conditionJmpAdrChild.jmpTrueAdr + 1 );
	}
	if ( conditionJmpAdrParent.jmpFalseAdr != 0 ) {
	    Code.put2( conditionJmpAdrChild.jmpFalseAdr,
		    conditionJmpAdrParent.jmpFalseAdr - conditionJmpAdrChild.jmpFalseAdr + 1 );
	}
    }

    public void visit( CondFactRelation condFactRelation ) {
	fixCondFact( conditionJmps.get( condFactRelation.getParent() ), conditionJmps.get( condFactRelation ) );
    }

    public void visit( CondFactNoRelation condFactNoRelation ) {
	fixCondFact( conditionJmps.get( condFactNoRelation.getParent() ), conditionJmps.get( condFactNoRelation ) );
    }
}
