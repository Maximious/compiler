package rs.ac.bg.etf.pp1;

import java.util.HashMap;
import java.util.Stack;

import rs.ac.bg.etf.pp1.BreakContinueSolver.OP_TYPE;
import rs.ac.bg.etf.pp1.ConditionVisitor.ConditionJmpAdr;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class CodeGenerator extends VisitorAdaptor {

    private int mainPc;

    public int getMainPc() {
	return mainPc;
    }

    private void generateEnrtyForFunction( Obj methodObj ) {
	Code.put( Code.enter );
	Code.put( methodObj.getLevel() );
	Code.put( methodObj.getLocalSymbols().size() );
    }

    private void registerReturn() {
	Code.put( Code.exit );
	Code.put( Code.return_ );
	returnFound = true;
    }

    // predeclared functions
    public void visit( ProgramName programName ) {
	Obj methodLen = Tab.find( "len" );
	Obj methodChr = Tab.find( "chr" );
	Obj methodOrd = Tab.find( "ord" );

	methodLen.setAdr( Code.pc );
	generateEnrtyForFunction( methodLen );
	Code.put( Code.load_n );
	Code.put( Code.arraylength );
	registerReturn();

	methodChr.setAdr( Code.pc );
	generateEnrtyForFunction( methodChr );
	Code.put( Code.load_n );
	registerReturn();

	methodOrd.setAdr( Code.pc );
	generateEnrtyForFunction( methodOrd );
	Code.put( Code.load_n );
	registerReturn();
    }

    private boolean returnFound;

    public void visit( MethodTypeName methodTypeName ) {
	if ( "main".equalsIgnoreCase( methodTypeName.getMethName() ) ) {
	    mainPc = Code.pc;
	}
	methodTypeName.obj.setAdr( Code.pc );
	generateEnrtyForFunction( methodTypeName.obj );

	returnFound = false;
    }

    public void visit( MethodDeclaration methodDecl ) {
	if ( !returnFound ) {
	    Code.put( Code.exit );
	    Code.put( Code.return_ );
	}
    }

    public void visit( ReturnExpr returnExpr ) {
	registerReturn();
    }

    public void visit( ReturnNoExpr returnNoExpr ) {
	registerReturn();
    }

    // load and store
    private void prepareArrRefLoad( Obj obj ) {
	Code.loadConst( obj.getFpPos() );
	if ( obj.getType().getKind() == Struct.Char ) {
	    Code.loadConst( 4 );
	    Code.put( Code.mul );
	}
	Code.put( Code.add );
    }

    private void prepareArrRefStore( Obj obj ) {
	Code.put( Code.dup_x1 );
	Code.put( Code.pop );

	Code.loadConst( obj.getFpPos() );
	if ( obj.getType().getKind() == Struct.Char ) {
	    Code.loadConst( 4 );
	    Code.put( Code.mul );
	}
	Code.put( Code.add );

	Code.put( Code.dup_x1 );
	Code.put( Code.pop );
    }

    private void loadOjb( Obj obj ) {
	if ( obj.getKind() == Obj.Elem && obj.getFpPos() > 1 ) {
	    prepareArrRefLoad( obj );
	}
	Code.load( obj );
    }

    private void storeObj( Obj obj ) {
	if ( obj.getKind() == Obj.Elem && obj.getFpPos() > 1 ) {
	    prepareArrRefStore( obj );
	}
	Code.store( obj );
    }

    // designator
    private Stack<Obj> desNames = new Stack<Obj>();

    public void visit( DesignatorClass designator ) {
	if ( !( designator.getParent() instanceof DesignatorClass ) ) {
	    desNames.pop();
	}
    }

    public void visit( SimpleDesignatorClass designator ) {
	if ( !( designator.getParent() instanceof DesignatorClass ) ) {
	    desNames.pop();
	}
    }

    public void visit( DesName desName ) {
	boolean isArray = ( (SimpleDesignator) desName.getParent() )
		.getArrRef().integer > 0;
	if ( isArray ) {
	    Code.load( desName.obj );
	}
	desNames.push( desName.obj );
    }

    public void visit( ArrayRefList arrayRefList ) {
	// n-dimensional arrays
	SyntaxNode currParent = arrayRefList.getParent();
	while ( currParent instanceof ArrayRefList ) {
	    Code.load( desNames.peek() );
	    Code.loadConst(
		    ( (ArrayRefList) ( currParent ) ).getArrRef().integer );
	    Code.put( Code.aload );// size
	    Code.put( Code.mul );
	    currParent = currParent.getParent();
	}
	if ( arrayRefList.integer > 1 ) {
	    Code.put( Code.add );
	}
    }

    public void visit( ReferencingDot refDot ) {
	loadOjb( ( (DesignatorClass) refDot.getParent() ).getDesignator().obj );
    }

    // designatorStatement
    private boolean checkDesignatorArrFld( Obj desObj ) {
	return desObj.getKind() == Obj.Elem || desObj.getKind() == Obj.Fld;
    }

    public void visit( PlusPlus plusPlus ) {
	Obj obj = plusPlus.getDesignator().obj;
	if ( checkDesignatorArrFld( obj ) ) {
	    Code.put( Code.dup2 );
	}
	loadOjb( obj );
	Code.loadConst( 1 );
	Code.put( Code.add );
	storeObj( obj );
    }

    public void visit( MinusMinus minusMinus ) {
	Obj obj = minusMinus.getDesignator().obj;
	if ( checkDesignatorArrFld( obj ) ) {
	    Code.put( Code.dup2 );
	}
	loadOjb( obj );
	Code.loadConst( -1 );
	Code.put( Code.add );
	storeObj( obj );
    }

    public void visit( Assign assign ) {
	storeObj( assign.getDesignator().obj );
    }

    // singleStatement
    public void visit( Read read ) {
	if ( read.getDesignator().obj.getType().getKind() == Struct.Char
		|| ( read.getDesignator().obj.getType()
			.getKind() == Struct.Array
			&& read.getDesignator().obj.getType().getElemType()
				.getKind() == Struct.Char ) ) {
	    Code.put( Code.bread );
	} else {
	    Code.put( Code.read );
	}
	storeObj( read.getDesignator().obj );
    }

    public void visit( PrintConst printConst ) {
	Code.loadConst( printConst.getN1() );
	if ( printConst.getExpr().struct.getKind() == Struct.Char ) {
	    Code.put( Code.bprint );
	} else {
	    Code.put( Code.print );
	}
    }

    public void visit( PrintNoConst printNoConst ) {
	if ( printNoConst.getExpr().struct.getKind() == Struct.Char ) {
	    Code.loadConst( 1 );
	    Code.put( Code.bprint );
	} else {
	    Code.loadConst( 4 );
	    Code.put( Code.print );
	}
    }

    public void visit( AllocationExpr allocationExpr ) {
	if ( allocationExpr.getArrRefAlloc().integer == 0 ) {
	    // record
	    int size = allocationExpr.struct.getMembers().size();
	    Code.put( Code.new_ );
	    Code.put2( size * 4 );
	} else {
	    boolean oneD = allocationExpr.getArrRefAlloc().integer == 1;
	    int arrType = allocationExpr.getType().struct
		    .getKind() == Struct.Char ? 0 : 1;

	    if ( !oneD ) {
		Code.loadConst( allocationExpr.getArrRefAlloc().integer );
		if ( arrType == 0 ) {
		    // char * sizeofWord
		    Code.loadConst( 4 );
		    Code.put( Code.mul );
		}
		Code.put( Code.add );
	    }

	    Code.put( Code.newarray );
	    Code.put( arrType );

	    if ( !oneD ) {
		for ( int i = allocationExpr.getArrRefAlloc().integer
			- 1; i >= 0; i-- ) {
		    Code.put( Code.dup_x1 );
		    Code.put( Code.dup_x1 );
		    Code.put( Code.pop );
		    Code.loadConst( i );
		    Code.put( Code.dup_x1 );
		    Code.put( Code.pop );
		    Code.put( Code.astore );
		}
	    }
	}
    }

    public void visit( ArrayRefListAlloc arrayRefListAlloc ) {
	if ( !( arrayRefListAlloc.getParent() instanceof ArrayRefListAlloc )
		&& arrayRefListAlloc.integer == 1 )
	    return;

	if ( arrayRefListAlloc.integer > 1 ) {
	    Code.put( Code.dup_x1 );
	    Code.put( Code.mul );
	} else {
	    Code.put( Code.dup );
	}
    }

    // expr, term && factor
    public void visit( DesignatorFactorNoParams designatorFactorNoParams ) {
	loadOjb( designatorFactorNoParams.getDesignator().obj );
    }

    public void visit( ConstNumFactor constNumFactor ) {
	Code.loadConst( constNumFactor.getVal() );
    }

    public void visit( ConstCharFactor constCharFactor ) {
	Code.loadConst( constCharFactor.getVal() );
    }

    public void visit( ConstBoolFactor constBoolFactor ) {
	int val = constBoolFactor.getVal() ? 1 : 0;
	Code.loadConst( val );
    }

    private void addMulop( Mulop mulop ) {
	if ( mulop instanceof Divop ) {
	    Code.put( Code.div );
	} else if ( mulop instanceof Moduop ) {
	    Code.put( Code.rem );
	} else {
	    Code.put( Code.mul );
	}
    }

    private void addAddop( Addop addop ) {
	if ( addop instanceof Subop ) {
	    Code.put( Code.sub );
	} else {
	    Code.put( Code.add );
	}
    }

    public void visit( MulopFactor mulopFactor ) {
	addMulop( mulopFactor.getMulop() );
    }

    public void visit( MulopFactorSingle mulopFactorSingle ) {
	addMulop( mulopFactorSingle.getMulop() );
    }

    public void visit( AddExpressionList addExpressionList ) {
	addAddop( addExpressionList.getAddop() );
    }

    public void visit( SingleAddExpressionList singleAddExpressionList ) {
	addAddop( singleAddExpressionList.getAddop() );
    }

    public void visit( MinusTermExpr minusTermExpr ) {
	Code.put( Code.neg );
    }

    // Condition
    private ConditionVisitor condVisitor = new ConditionVisitor();
    private int currentConditionLocation = 0;

    private HashMap<SyntaxNode, Integer> elseJmps = new HashMap<SyntaxNode, Integer>();
    private BreakContinueSolver breakContinueSolver = new BreakContinueSolver();

    public void visit( IfStart ifStart ) {
	currentConditionLocation = Code.pc;
    }

    public void visit( WhileStart whileStart ) {
	currentConditionLocation = Code.pc;
    }

    public void visit( DoStart doWhileStart ) {
	condVisitor.addCondition( doWhileStart.getParent(),
		new ConditionJmpAdr( Code.pc, Code.pc, 0 ) );
	breakContinueSolver.doStarted();
    }

    public void visit( IfConditionClass conditionEnd ) {
	condVisitor.addCondition( conditionEnd.getParent(),
		new ConditionJmpAdr( 0, Code.pc, 0 ) );
	condVisitor.addCondition( conditionEnd,
		new ConditionJmpAdr( 0, Code.pc, 0 ) );
    }

    private void solveConditions( SyntaxNode node ) {

	if ( node instanceof Do ) {
	    ( (Do) node ).getCondition().traverseTopDown( condVisitor );
	    return;
	}

	IfCondition condition;
	if ( node instanceof IfUnmatched ) {
	    condition = ( (IfUnmatched) node ).getIfCondition();
	} else if ( node instanceof IfElseUnmatched ) {
	    condition = ( (IfElseUnmatched) node ).getIfCondition();
	} else {
	    condition = ( (IfElseMatched) node ).getIfCondition();
	}

	condition.traverseTopDown( condVisitor );

	// condVisitor.resetConditionJmps();
    }

    public void visit( ElseCond elseCond ) {
	Code.putJump( 0 );
	elseJmps.put( elseCond.getParent(), Code.pc - 2 );
	condVisitor.getCondition( elseCond.getParent() ).jmpFalseAdr = Code.pc;
	solveConditions( elseCond.getParent() );
    }

    public void visit( IfElseMatched ifElseMatched ) {
	Integer jmpAdr = elseJmps.get( ifElseMatched );
	Code.put2( jmpAdr, Code.pc - jmpAdr + 1 );
    }

    public void visit( IfElseUnmatched ifElseUnmatched ) {
	Integer jmpAdr = elseJmps.get( ifElseUnmatched );
	Code.put2( jmpAdr, Code.pc - jmpAdr + 1 );
    }

    public void visit( IfUnmatched ifUnmatched ) {
	condVisitor.getCondition( ifUnmatched ).jmpFalseAdr = Code.pc;
	solveConditions( ifUnmatched );
    }

    public void visit( Do doClass ) {
	ConditionJmpAdr doInfo = condVisitor.getCondition( doClass );
	ConditionJmpAdr condInfo = condVisitor
		.getCondition( doClass.getCondition() );
	doInfo.jmpFalseAdr = Code.pc;
	solveConditions( doClass );
	breakContinueSolver.solveDo( condInfo.conditionStartAdr,
		doInfo.jmpFalseAdr );
    }

    public void visit( Break breakClass ) {
	breakContinueSolver.addOp( OP_TYPE.BREAK, Code.pc + 1 );
	Code.putJump( 0 );
    }

    public void visit( Continue continueClass ) {
	breakContinueSolver.addOp( OP_TYPE.CONTINUE, Code.pc + 1 );
	Code.putJump( 0 );
    }

    private void putRelOp( Relop relop, SyntaxNode node ) {
	int relopCode;

	if ( relop instanceof RelEqual ) {
	    relopCode = Code.eq;
	} else if ( relop instanceof NotEqual ) {
	    relopCode = Code.ne;
	} else if ( relop instanceof GreatherThan ) {
	    relopCode = Code.gt;
	} else if ( relop instanceof GreatherEqualThan ) {
	    relopCode = Code.ge;
	} else if ( relop instanceof LessThan ) {
	    relopCode = Code.lt;
	} else {
	    relopCode = Code.le;
	}

	int trueJmpPC = Code.pc + 1;
	Code.putFalseJump( Code.inverse[relopCode], Code.pc + 6 );
	int falseJmpPC = Code.pc + 1;
	Code.putJump( Code.pc + 3 );

	condVisitor.addCondition( node, new ConditionJmpAdr(
		currentConditionLocation, trueJmpPC, falseJmpPC ) );

	currentConditionLocation = Code.pc;
    }

    public void visit( ConditionClass conditionClass ) {
	int currentLocation = condVisitor.getCondition(
		conditionClass.getCondition() ).conditionStartAdr;
	condVisitor.addCondition( conditionClass,
		new ConditionJmpAdr( currentLocation, 0, 0 ) );
    }

    public void visit( BasicCondTerm basicCondTerm ) {
	int currentLocation = condVisitor
		.getCondition( basicCondTerm.getCondTerm() ).conditionStartAdr;
	condVisitor.addCondition( basicCondTerm,
		new ConditionJmpAdr( currentLocation, 0, 0 ) );
    }

    public void visit( ConditionTerm conditionTerm ) {
	int currentLocation = condVisitor
		.getCondition( conditionTerm.getCondTerm() ).conditionStartAdr;
	condVisitor.addCondition( conditionTerm,
		new ConditionJmpAdr( currentLocation, 0, 0 ) );
    }

    public void visit( BasicCondFact basicCondFact ) {
	int currentLocation = condVisitor
		.getCondition( basicCondFact.getCondFact() ).conditionStartAdr;
	condVisitor.addCondition( basicCondFact,
		new ConditionJmpAdr( currentLocation, 0, 0 ) );
    }

    public void visit( CondFactRelation condFactRelation ) {
	putRelOp( condFactRelation.getRelop(), condFactRelation );
    }

    public void visit( CondFactNoRelation condFactNoRelation ) {
	Code.loadConst( 0 );
	putRelOp( new NotEqual(), condFactNoRelation );
    }

    // functions
    private void callFunction( Obj functionObj ) {
	int offset = functionObj.getAdr() - Code.pc;
	Code.put( Code.call );

	Code.put2( offset );
	if ( functionObj.getType().getKind() != Struct.None ) {
	    Code.put( Code.pop );
	}
    }

    public void visit( CallProc callProc ) {
	callFunction( callProc.getDesignator().obj );
    }

    public void visit( CallProcEmpty callProcEmpty ) {
	callFunction( callProcEmpty.getDesignator().obj );
    }

    public void visit( DesignatorFactorParams designatorFactorParams ) {
	Obj functionObj = designatorFactorParams.getDesignator().obj;
	int offset = functionObj.getAdr() - Code.pc;
	Code.put( Code.call );

	Code.put2( offset );
    }
}
