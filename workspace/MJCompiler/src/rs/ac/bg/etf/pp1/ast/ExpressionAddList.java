// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class ExpressionAddList extends Expr {

    private TermExpr TermExpr;
    private AddExprList AddExprList;

    public ExpressionAddList (TermExpr TermExpr, AddExprList AddExprList) {
        this.TermExpr=TermExpr;
        if(TermExpr!=null) TermExpr.setParent(this);
        this.AddExprList=AddExprList;
        if(AddExprList!=null) AddExprList.setParent(this);
    }

    public TermExpr getTermExpr() {
        return TermExpr;
    }

    public void setTermExpr(TermExpr TermExpr) {
        this.TermExpr=TermExpr;
    }

    public AddExprList getAddExprList() {
        return AddExprList;
    }

    public void setAddExprList(AddExprList AddExprList) {
        this.AddExprList=AddExprList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(TermExpr!=null) TermExpr.accept(visitor);
        if(AddExprList!=null) AddExprList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(TermExpr!=null) TermExpr.traverseTopDown(visitor);
        if(AddExprList!=null) AddExprList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(TermExpr!=null) TermExpr.traverseBottomUp(visitor);
        if(AddExprList!=null) AddExprList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ExpressionAddList(\n");

        if(TermExpr!=null)
            buffer.append(TermExpr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AddExprList!=null)
            buffer.append(AddExprList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ExpressionAddList]");
        return buffer.toString();
    }
}
