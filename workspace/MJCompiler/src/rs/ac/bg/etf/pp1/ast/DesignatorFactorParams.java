// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class DesignatorFactorParams extends Factor {

    private Designator Designator;
    private FactorDesPar FactorDesPar;

    public DesignatorFactorParams (Designator Designator, FactorDesPar FactorDesPar) {
        this.Designator=Designator;
        if(Designator!=null) Designator.setParent(this);
        this.FactorDesPar=FactorDesPar;
        if(FactorDesPar!=null) FactorDesPar.setParent(this);
    }

    public Designator getDesignator() {
        return Designator;
    }

    public void setDesignator(Designator Designator) {
        this.Designator=Designator;
    }

    public FactorDesPar getFactorDesPar() {
        return FactorDesPar;
    }

    public void setFactorDesPar(FactorDesPar FactorDesPar) {
        this.FactorDesPar=FactorDesPar;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Designator!=null) Designator.accept(visitor);
        if(FactorDesPar!=null) FactorDesPar.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Designator!=null) Designator.traverseTopDown(visitor);
        if(FactorDesPar!=null) FactorDesPar.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Designator!=null) Designator.traverseBottomUp(visitor);
        if(FactorDesPar!=null) FactorDesPar.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignatorFactorParams(\n");

        if(Designator!=null)
            buffer.append(Designator.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(FactorDesPar!=null)
            buffer.append(FactorDesPar.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignatorFactorParams]");
        return buffer.toString();
    }
}
