// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class ClassDefinition extends ClassDef {

    private ClassDefSec ClassDefSec;

    public ClassDefinition (ClassDefSec ClassDefSec) {
        this.ClassDefSec=ClassDefSec;
        if(ClassDefSec!=null) ClassDefSec.setParent(this);
    }

    public ClassDefSec getClassDefSec() {
        return ClassDefSec;
    }

    public void setClassDefSec(ClassDefSec ClassDefSec) {
        this.ClassDefSec=ClassDefSec;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassDefSec!=null) ClassDefSec.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassDefSec!=null) ClassDefSec.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassDefSec!=null) ClassDefSec.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDefinition(\n");

        if(ClassDefSec!=null)
            buffer.append(ClassDefSec.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDefinition]");
        return buffer.toString();
    }
}
