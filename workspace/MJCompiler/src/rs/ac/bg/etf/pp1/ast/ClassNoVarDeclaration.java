// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class ClassNoVarDeclaration extends ClassDeclSec {

    private ClassDef ClassDef;

    public ClassNoVarDeclaration (ClassDef ClassDef) {
        this.ClassDef=ClassDef;
        if(ClassDef!=null) ClassDef.setParent(this);
    }

    public ClassDef getClassDef() {
        return ClassDef;
    }

    public void setClassDef(ClassDef ClassDef) {
        this.ClassDef=ClassDef;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassDef!=null) ClassDef.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassDef!=null) ClassDef.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassDef!=null) ClassDef.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassNoVarDeclaration(\n");

        if(ClassDef!=null)
            buffer.append(ClassDef.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassNoVarDeclaration]");
        return buffer.toString();
    }
}
