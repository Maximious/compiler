// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class MethodDeclVars extends MethodDeclSec {

    private VarDeclList VarDeclList;
    private MethodDefStart MethodDefStart;
    private MethodDef MethodDef;

    public MethodDeclVars (VarDeclList VarDeclList, MethodDefStart MethodDefStart, MethodDef MethodDef) {
        this.VarDeclList=VarDeclList;
        if(VarDeclList!=null) VarDeclList.setParent(this);
        this.MethodDefStart=MethodDefStart;
        if(MethodDefStart!=null) MethodDefStart.setParent(this);
        this.MethodDef=MethodDef;
        if(MethodDef!=null) MethodDef.setParent(this);
    }

    public VarDeclList getVarDeclList() {
        return VarDeclList;
    }

    public void setVarDeclList(VarDeclList VarDeclList) {
        this.VarDeclList=VarDeclList;
    }

    public MethodDefStart getMethodDefStart() {
        return MethodDefStart;
    }

    public void setMethodDefStart(MethodDefStart MethodDefStart) {
        this.MethodDefStart=MethodDefStart;
    }

    public MethodDef getMethodDef() {
        return MethodDef;
    }

    public void setMethodDef(MethodDef MethodDef) {
        this.MethodDef=MethodDef;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclList!=null) VarDeclList.accept(visitor);
        if(MethodDefStart!=null) MethodDefStart.accept(visitor);
        if(MethodDef!=null) MethodDef.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclList!=null) VarDeclList.traverseTopDown(visitor);
        if(MethodDefStart!=null) MethodDefStart.traverseTopDown(visitor);
        if(MethodDef!=null) MethodDef.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclList!=null) VarDeclList.traverseBottomUp(visitor);
        if(MethodDefStart!=null) MethodDefStart.traverseBottomUp(visitor);
        if(MethodDef!=null) MethodDef.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MethodDeclVars(\n");

        if(VarDeclList!=null)
            buffer.append(VarDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDefStart!=null)
            buffer.append(MethodDefStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDef!=null)
            buffer.append(MethodDef.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MethodDeclVars]");
        return buffer.toString();
    }
}
