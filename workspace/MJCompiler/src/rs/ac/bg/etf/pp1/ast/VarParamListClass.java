// generated with ast extension for cup
// version 0.8
// 30/0/2022 22:5:33


package rs.ac.bg.etf.pp1.ast;

public class VarParamListClass extends VarParamList {

    private VarParamList VarParamList;
    private String varName;
    private Arr Arr;

    public VarParamListClass (VarParamList VarParamList, String varName, Arr Arr) {
        this.VarParamList=VarParamList;
        if(VarParamList!=null) VarParamList.setParent(this);
        this.varName=varName;
        this.Arr=Arr;
        if(Arr!=null) Arr.setParent(this);
    }

    public VarParamList getVarParamList() {
        return VarParamList;
    }

    public void setVarParamList(VarParamList VarParamList) {
        this.VarParamList=VarParamList;
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName=varName;
    }

    public Arr getArr() {
        return Arr;
    }

    public void setArr(Arr Arr) {
        this.Arr=Arr;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarParamList!=null) VarParamList.accept(visitor);
        if(Arr!=null) Arr.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarParamList!=null) VarParamList.traverseTopDown(visitor);
        if(Arr!=null) Arr.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarParamList!=null) VarParamList.traverseBottomUp(visitor);
        if(Arr!=null) Arr.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarParamListClass(\n");

        if(VarParamList!=null)
            buffer.append(VarParamList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+varName);
        buffer.append("\n");

        if(Arr!=null)
            buffer.append(Arr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarParamListClass]");
        return buffer.toString();
    }
}
