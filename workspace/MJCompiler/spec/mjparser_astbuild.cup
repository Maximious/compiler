package rs.ac.bg.etf.pp1;

import java_cup.runtime.*;
import org.apache.log4j.*;
import rs.ac.bg.etf.pp1.ast.*;

parser code {:
	
	boolean errorDetected = false;	
	Logger log = Logger.getLogger(getClass());   
   
    // slede redefinisani metodi za prijavu gresaka radi izmene teksta poruke
     
    public void report_fatal_error(String message, Object info) throws java.lang.Exception {
      done_parsing();
      report_error(message, info);
    }
  
    public void syntax_error(Symbol cur_token) {
        report_error("Sintaksna greska", cur_token);
    }
  
    public void unrecovered_syntax_error(Symbol cur_token) throws java.lang.Exception {
        report_fatal_error("Fatalna greska, parsiranje se ne moze nastaviti", cur_token);
    }

    public void report_error(String message, Object info) {
    	errorDetected = true;
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.error(msg.toString());
    }
    
    public void report_info(String message, Object info) {
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.info(msg.toString());
    }
    
:}

scan with {:
	Symbol s = this.getScanner().next_token();
	if (s != null && s.value != null) 
		log.info(s.toString() + " " + s.value.toString());
	return s;
:}

terminal EQUAL, PLUS, MINUS, MUL, DIV, MODUO, LEQL, NEQL, GRT, GRTE, LT, LTE; 
terminal PROG, BREAK, CLASS, ENUM, ELSE, CONST, IF, DO, WHILE, NEW, RECORD;
terminal PRINT, READ, RETURN, VOID, EXTENDS, CONTINUE, THIS, SUPER, GOTO; 
terminal AND, OR, PP, MM, SEMI, COMMA, LPAREN, RPAREN, LBRACE, RBRACE, COLON, DOT, LSQU, RSQU; 
terminal Integer NUMBER;
terminal String IDENT;
terminal Character CHAR;
terminal Boolean BOOL;

nonterminal ProgramMet ProgramMet;
nonterminal DeclList DeclList;
nonterminal VarParamList VarParamList;
nonterminal PossDecl PossDecl;
nonterminal VarDeclList VarDeclList;
nonterminal VarDecl VarDecl;
nonterminal ConstDecl ConstDecl;
nonterminal ConstList ConstList;
nonterminal RecDecl RecDecl;
nonterminal ClassDecl ClassDecl;
nonterminal ClassName ClassName;
nonterminal ClassDeclSec ClassDeclSec;
nonterminal ClassDef ClassDef;
nonterminal ClassDefSec ClassDefSec;
nonterminal ClassDefThrd ClassDefThrd; 
nonterminal ConstructorDecl ConstructorDecl;
nonterminal ConstructorDeclSec ConstructorDeclSec;
nonterminal MethodDeclList MethodDeclList;
nonterminal MethodDecl MethodDecl;
nonterminal MethodDeclSec MethodDeclSec;
nonterminal MethodDef MethodDef;
nonterminal FormPars FormPars;
nonterminal FormParsDecl FormParsDecl;
nonterminal FormalParamDecl FormalParamDecl;
nonterminal StatementList StatementList;
nonterminal Statement Statement;
nonterminal Statements Statements;
nonterminal DesignatorStatement DesignatorStatement; 
nonterminal FactorDesPar FactorDesPar;
nonterminal IfStart IfStart;
nonterminal ElseCond ElseCond;
nonterminal DoWhileStart DoWhileStart;
nonterminal WhileStart WhileStart;
nonterminal Assignop Assignop;
nonterminal Relop Relop;
nonterminal Addop Addop;
nonterminal Mulop Mulop;
nonterminal MethodDefStart MethodDefStart;
nonterminal RefDot RefDot;
nonterminal Matched Matched;
nonterminal Unmatched Unmatched; /*, Label, LabeledStatement, SingleStatement */

nonterminal java.lang.Integer ArrRef, ArrRefAlloc;
nonterminal rs.etf.pp1.symboltable.concepts.Obj Program, ProgramName, RecordName, MethodTypeName, ActPars; 
nonterminal rs.etf.pp1.symboltable.concepts.Obj Designator, SimpleDesignator, DesName;
nonterminal rs.etf.pp1.symboltable.concepts.Struct Type, Arr, MethodRetType, AssignExpr;
nonterminal rs.etf.pp1.symboltable.concepts.Struct Expr, Factor, Term, ConstVal, IfCondition;
nonterminal rs.etf.pp1.symboltable.concepts.Struct MulFactor, TermExpr, AddExprList, Condition, CondFact, CondTerm;

Program ::= (Program) PROG ProgramName:name DeclList:D1 LBRACE ProgramMet:P2 {: RESULT=new Program(name, D1, P2); RESULT.setLine(nameleft); :};			
			
ProgramName ::= (ProgramName) IDENT:pName {: RESULT=new ProgramName(pName); RESULT.setLine(pNameleft); :};

ProgramMet ::= (ProgramMethodDeclarations) MethodDeclList:M1 RBRACE {: RESULT=new ProgramMethodDeclarations(M1); RESULT.setLine(M1left); :}
				 |
			   (NoProgramMethodDeclarations) RBRACE {: RESULT=new NoProgramMethodDeclarations(); :};		

DeclList ::= (Declarations) DeclList:D1 PossDecl:P2 {: RESULT=new Declarations(D1, P2); RESULT.setLine(D1left); :}
				|
			 (NoDeclaration) {: RESULT=new NoDeclaration(); :}; /*epsilon*/			 

PossDecl ::= (PossVarDeclaration) VarDecl:V1 {: RESULT=new PossVarDeclaration(V1); RESULT.setLine(V1left); :}				  
				|
			 (PossConstDeclaration) ConstDecl:C1 {: RESULT=new PossConstDeclaration(C1); RESULT.setLine(C1left); :}
				|
   		     (PossRecDeclaration) RecDecl:R1 {: RESULT=new PossRecDeclaration(R1); RESULT.setLine(R1left); :}		
				|
			 (PossClassDeclaration) ClassDecl:C1 {: RESULT=new PossClassDeclaration(C1); RESULT.setLine(C1left); :};		
							

VarDeclList ::= (VarDeclarations) VarDeclList:V1 VarDecl:V2 {: RESULT=new VarDeclarations(V1, V2); RESULT.setLine(V1left); :}
					|
				(SingleVarDeclaration) VarDecl:V1 {: RESULT=new SingleVarDeclaration(V1); RESULT.setLine(V1left); :};
				
VarDecl ::= (VarDeclarationSingle) Type:varType IDENT:varName Arr:arrType SEMI {: RESULT=new VarDeclarationSingle(varType, varName, arrType); RESULT.setLine(varTypeleft); :}
				|
			(VarDeclarationMany) VarParamList:V1 IDENT:varName Arr:arrType SEMI {: RESULT=new VarDeclarationMany(V1, varName, arrType); RESULT.setLine(V1left); :}
				|			
			(VarDeclarationError) error SEMI:l
			{: parser.report_error( "Izvrsen oporavak do kraja deklaracije promenljivih;", lleft ); :} {: RESULT=new VarDeclarationError(); :};			

VarParamList ::= (VarParamListClass) VarParamList:V1 IDENT:varName Arr:arrType COMMA {: RESULT=new VarParamListClass(V1, varName, arrType); RESULT.setLine(V1left); :}
					|
				 (VarParamListStart) Type:varType IDENT:varName Arr:arrType COMMA {: RESULT=new VarParamListStart(varType, varName, arrType); RESULT.setLine(varTypeleft); :}
					|
				 (VarParamListError) error COMMA:l
			     {: parser.report_error( "Izvrsen oporavak do sledece promenljive.", lleft ); :} {: RESULT=new VarParamListError(); :};

Arr ::= (Array) Arr:arrType LSQU RSQU {: RESULT=new Array(arrType); RESULT.setLine(arrTypeleft); :}
		  |
		(NoArrDecl) {: RESULT=new NoArrDecl(); :};/* epsilon */		
			
Type ::= (Type) IDENT:typeName {: RESULT=new Type(typeName); RESULT.setLine(typeNameleft); :};


ConstDecl ::= (ConstantDeclaration) CONST Type:varType ConstList:C1 SEMI {: RESULT=new ConstantDeclaration(varType, C1); RESULT.setLine(varTypeleft); :};
					
ConstList ::= (ConstListClass) ConstList:C1 COMMA IDENT:varName EQUAL ConstVal:C2 {: RESULT=new ConstListClass(C1, varName, C2); RESULT.setLine(C1left); :}
				|			
			  (SingleConst) IDENT:varName EQUAL ConstVal:C1 {: RESULT=new SingleConst(varName, C1); RESULT.setLine(varNameleft); :};	
			  		
ConstVal ::= (ConstBool) BOOL:b {: RESULT=new ConstBool(b); RESULT.setLine(bleft); :}
				|
			 (ConstNum) NUMBER:num {: RESULT=new ConstNum(num); RESULT.setLine(numleft); :}
				|
			 (ConstChar) CHAR:ch {: RESULT=new ConstChar(ch); RESULT.setLine(chleft); :};
			 
			 
RecDecl ::= (RecordDeclarationsList) RECORD RecordName:rName LBRACE VarDeclList:V1 RBRACE {: RESULT=new RecordDeclarationsList(rName, V1); RESULT.setLine(rNameleft); :}
				|
			(RecordNoDeclarationsList) RECORD RecordName:rName LBRACE RBRACE {: RESULT=new RecordNoDeclarationsList(rName); RESULT.setLine(rNameleft); :};

RecordName ::= (RecordName) IDENT:rName {: RESULT=new RecordName(rName); RESULT.setLine(rNameleft); :};				
		
				
ClassDecl ::= (ClassExtend) CLASS ClassName:nm EXTENDS Type:t LBRACE ClassDeclSec:C1 {: RESULT=new ClassExtend(nm, t, C1); RESULT.setLine(nmleft); :}
				|
			  (ClassNoExtend) CLASS ClassName:nm LBRACE ClassDeclSec:C1 {: RESULT=new ClassNoExtend(nm, C1); RESULT.setLine(nmleft); :};
			  
ClassDeclSec ::= (ClassVarDeclaration)  VarDeclList:V1 ClassDef:C2 {: RESULT=new ClassVarDeclaration(V1, C2); RESULT.setLine(V1left); :}
					|			 			
				 (ClassNoVarDeclaration) ClassDef:C1 {: RESULT=new ClassNoVarDeclaration(C1); RESULT.setLine(C1left); :};
				 
ClassDef ::= (ClassDefinition) ClassDefSec:C1 RBRACE {: RESULT=new ClassDefinition(C1); RESULT.setLine(C1left); :}
				|
			 (NoClassDefinition) RBRACE {: RESULT=new NoClassDefinition(); :};
			 			 
ClassDefSec ::= (ConstructorClass) LBRACE ConstructorDecl:C1 ClassDefThrd:C2 {: RESULT=new ConstructorClass(C1, C2); RESULT.setLine(C1left); :} 
					|
				(NoConstructorClass) LBRACE ClassDefThrd:C1 {: RESULT=new NoConstructorClass(C1); RESULT.setLine(C1left); :};

ClassDefThrd ::= (ClassMethods) MethodDeclList:M1 RBRACE {: RESULT=new ClassMethods(M1); RESULT.setLine(M1left); :}
					|
				 (NoClassMethods) RBRACE {: RESULT=new NoClassMethods(); :};
				 
ClassName ::= (ClassName) IDENT:name {: RESULT=new ClassName(name); RESULT.setLine(nameleft); :};				 
			 
ConstructorDecl ::= (ConstructorVarDeclaration) IDENT:nm LPAREN RPAREN VarDeclList:V1 LBRACE ConstructorDeclSec:C2 {: RESULT=new ConstructorVarDeclaration(nm, V1, C2); RESULT.setLine(nmleft); :}
						|				
				  	(ConstructorNoVarDeclaration) IDENT:nm LPAREN RPAREN LBRACE ConstructorDeclSec:C1 {: RESULT=new ConstructorNoVarDeclaration(nm, C1); RESULT.setLine(nmleft); :};
				  	
ConstructorDeclSec ::= (ClassStatements) StatementList:S1 RBRACE {: RESULT=new ClassStatements(S1); RESULT.setLine(S1left); :}
						 |
					   (EmptyClass) RBRACE {: RESULT=new EmptyClass(); :};

MethodDeclList ::= (MethodDeclarations) MethodDeclList:M1 MethodDecl:M2 {: RESULT=new MethodDeclarations(M1, M2); RESULT.setLine(M1left); :}
					 |
				   (SingleMethodDecl) MethodDecl:M1 {: RESULT=new SingleMethodDecl(M1); RESULT.setLine(M1left); :};
				   					
MethodDecl ::= (MethodDeclaration) MethodTypeName:tName LPAREN FormPars:F1 RPAREN MethodDeclSec:M2 {: RESULT=new MethodDeclaration(tName, F1, M2); RESULT.setLine(tNameleft); :};				

MethodDeclSec ::= (MethodDeclVars)  VarDeclList:V1 MethodDefStart:M2 MethodDef:M3 {: RESULT=new MethodDeclVars(V1, M2, M3); RESULT.setLine(V1left); :}   
					|
				  (NoMethodDeclVars) MethodDefStart:M1 MethodDef:M2 {: RESULT=new NoMethodDeclVars(M1, M2); RESULT.setLine(M1left); :};
				  
MethodDefStart ::= (MethodDefinitionStart) LBRACE {: RESULT=new MethodDefinitionStart(); :};				  
				  
MethodDef ::= (MethodDefinition) StatementList:S1 RBRACE {: RESULT=new MethodDefinition(S1); RESULT.setLine(S1left); :}
				|
			  (NoMethodDefinition) RBRACE {: RESULT=new NoMethodDefinition(); :};
				
MethodTypeName ::= (MethodTypeName) MethodRetType:retType IDENT:methName {: RESULT=new MethodTypeName(retType, methName); RESULT.setLine(retTypeleft); :};

MethodRetType ::= (MethodReturnType) Type:retType {: RESULT=new MethodReturnType(retType); RESULT.setLine(retTypeleft); :}
					|
				  (VoidClass) VOID {: RESULT=new VoidClass(); :};

FormPars ::= (NoFormPars) {: RESULT=new NoFormPars(); :} /*epsilon*/
				|
			 (FromParamsDecl) FormParsDecl:F1 FormalParamDecl:F2 {: RESULT=new FromParamsDecl(F1, F2); RESULT.setLine(F1left); :}
			 	|
			 (FromParamsError) error:l
			 {: parser.report_error( "Izvrsen oporavak formalnih parametara do ).", lleft ); :} {: RESULT=new FromParamsError(); :};
			 
FormParsDecl ::= (FormalParamDeclMany) FormParsDecl:F1 FormalParamDecl:F2 COMMA {: RESULT=new FormalParamDeclMany(F1, F2); RESULT.setLine(F1left); :}
				   |
			 	 (NoFormalParamDecl) {: RESULT=new NoFormalParamDecl(); :} /*epsilon*/
			 	   |
			 	 (FormalParamDeclError) error COMMA:l
			 	 {: parser.report_error( "Izvrsen oporavak formalnih parametara do , ", lleft ); :} {: RESULT=new FormalParamDeclError(); :};
			 				
FormalParamDecl ::= (FormalParamDecl) Type:type IDENT:name Arr:A1 {: RESULT=new FormalParamDecl(type, name, A1); RESULT.setLine(typeleft); :};


StatementList ::= (StatementListClass) StatementList:S1 Statement:S2 {: RESULT=new StatementListClass(S1, S2); RESULT.setLine(S1left); :} 
					|
				  (SingleStmt) Statement:S1 {: RESULT=new SingleStmt(S1); RESULT.setLine(S1left); :};

Statement ::= (MatchedClass) Matched:M1 {: RESULT=new MatchedClass(M1); RESULT.setLine(M1left); :}
				|
			  (UnmatchedClass) Unmatched:U1 {: RESULT=new UnmatchedClass(U1); RESULT.setLine(U1left); :};

Matched ::= (DesignatorStment) DesignatorStatement:D1 SEMI {: RESULT=new DesignatorStment(D1); RESULT.setLine(D1left); :}
				|
			(StatementStatements) Statements:S1 {: RESULT=new StatementStatements(S1); RESULT.setLine(S1left); :}			  
				|
		  	(Break) BREAK SEMI {: RESULT=new Break(); :}
				|
			(Continue) CONTINUE SEMI {: RESULT=new Continue(); :}
			 	| 
			(Do) DoWhileStart:D1 Statement:S2 WhileStart:W3 LPAREN Condition:C4 RPAREN SEMI {: RESULT=new Do(D1, S2, W3, C4); RESULT.setLine(D1left); :}
			 	|
			(ReturnExpr) RETURN Expr:type SEMI {: RESULT=new ReturnExpr(type); RESULT.setLine(typeleft); :}
				|
			(ReturnNoExpr) RETURN SEMI {: RESULT=new ReturnNoExpr(); :}
				|			
			(IfElseMatched) IfStart:I1 IfCondition:condType RPAREN Matched:M2 ElseCond:E3 Matched:M4 {: RESULT=new IfElseMatched(I1, condType, M2, E3, M4); RESULT.setLine(I1left); :}
			  	|
  		  	(Read) READ LPAREN Designator:desObj RPAREN SEMI {: RESULT=new Read(desObj); RESULT.setLine(desObjleft); :}
				| 
			(PrintConst) PRINT LPAREN Expr:type COMMA NUMBER:N1 RPAREN SEMI {: RESULT=new PrintConst(type, N1); RESULT.setLine(typeleft); :}
				| 
	  	    (PrintNoConst) PRINT LPAREN Expr:type RPAREN SEMI {: RESULT=new PrintNoConst(type); RESULT.setLine(typeleft); :};
	  	    
Unmatched ::= (IfUnmatched) IfStart:I1 IfCondition:condType RPAREN Statement:S2 {: RESULT=new IfUnmatched(I1, condType, S2); RESULT.setLine(I1left); :}
				| 	  	    
			  (IfElseUnmatched) IfStart:I1 IfCondition:condType RPAREN Matched:M2 ElseCond:E3 Unmatched:U4 {: RESULT=new IfElseUnmatched(I1, condType, M2, E3, U4); RESULT.setLine(I1left); :};

IfStart ::= (IfStart) IF {: RESULT=new IfStart(); :};

WhileStart ::= (WhileStart) WHILE {: RESULT=new WhileStart(); :};

ElseCond ::= (ElseCond) ELSE {: RESULT=new ElseCond(); :};

DoWhileStart ::= (DoStart) DO {: RESULT=new DoStart(); :};	

IfCondition ::= (IfConditionClass) LPAREN Condition:type {: RESULT=new IfConditionClass(type); RESULT.setLine(typeleft); :}
					|
				(ErrorCondition) LPAREN error:l				
			  	{: parser.log.debug("Uspesan oporavak od greske u if uslovu."); :} {: RESULT=new ErrorCondition(); :};
			  					
			  
/* //old declaration
Statement ::= (SingleStatementClass) LabeledStatement
			  	|
			  (StatementGroup) Statements;		   		 		   		   				 

LabeledStatement ::= (LabeledSingleStatement) Label:lName COLON SingleStatement
						|			  
					 (NonLabeledSingleStatement) SingleStatement;
					 
Label ::= (Label) IDENT:name;
		  							  				
SingleStatement ::= (MatchedClass) Matched
						|
					(UnmatchedClass) Unmatched;

Matched ::= (DesignatorStatementMatched) DesignatorStatement SEMI
				|
			(Break) BREAK SEMI
				|
			(Continue) CONTINUE SEMI			 
				| 
			(Goto) GOTO Label:lName SEMI
				|
			(ReturnExpr) RETURN Expr:type SEMI
				|
			(ReturnNoExpr) RETURN SEMI
		  	    |
	   		(Read) READ LPAREN Designator:desObj RPAREN SEMI
				|
			(PrintConst) PRINT LPAREN Expr:type COMMA NUMBER RPAREN SEMI
				|
			(PrintNoConst) PRINT LPAREN Expr:type RPAREN SEMI
				|
			(Do) DoWhileStart Statement WHILE LPAREN Condition RPAREN SEMI
			  	|
			(IfElse) IF LPAREN Condition RPAREN Matched ELSE Matched;							
	  		
Unmatched ::= (If) IF LPAREN Condition RPAREN Statement
				| 
			  (IfElseUnmatched) IF LPAREN Condition RPAREN Matched ELSE Unmatched;
*/				   	  		

Statements ::= (StatementsClass) LBRACE StatementList:S1 RBRACE {: RESULT=new StatementsClass(S1); RESULT.setLine(S1left); :}
				 |
			   (EmptyStatementsClass) LBRACE RBRACE {: RESULT=new EmptyStatementsClass(); :};	

DesignatorStatement ::= (Assign) Designator:desType Assignop:A1 AssignExpr:assignType {: RESULT=new Assign(desType, A1, assignType); RESULT.setLine(desTypeleft); :}
							|
					  	(CallProc) Designator:desObj LPAREN ActPars:A1 RPAREN {: RESULT=new CallProc(desObj, A1); RESULT.setLine(desObjleft); :}
						  	|
					  	(CallProcEmpty) Designator:desObj LPAREN RPAREN {: RESULT=new CallProcEmpty(desObj); RESULT.setLine(desObjleft); :}
						  	| 
					  	(PlusPlus) Designator:desObj PP {: RESULT=new PlusPlus(desObj); RESULT.setLine(desObjleft); :}
						  	|		
					  	(MinusMinus) Designator:desObj MM {: RESULT=new MinusMinus(desObj); RESULT.setLine(desObjleft); :};					  		
	  					
AssignExpr ::= (AssignExprClass) Expr:exprType {: RESULT=new AssignExprClass(exprType); RESULT.setLine(exprTypeleft); :}	  					
				 |
	 	       (ErrAssignment) error:errType
	  		   {: parser.log.debug("Uspesan oporavak od greske pri dodeli vrednosti."); :} {: RESULT=new ErrAssignment(); :};

ActPars ::= (ActParsClass) ActPars:A1 COMMA Expr:type {: RESULT=new ActParsClass(A1, type); RESULT.setLine(A1left); :}
				|
			(SingleActPar) Expr:type {: RESULT=new SingleActPar(type); RESULT.setLine(typeleft); :};			   			   

Condition ::= (ConditionClass) Condition:condType OR CondTerm:termType {: RESULT=new ConditionClass(condType, termType); RESULT.setLine(condTypeleft); :}
				|
			  (BasicCondTerm) CondTerm:condType {: RESULT=new BasicCondTerm(condType); RESULT.setLine(condTypeleft); :};			  			  			 			
			  
CondTerm ::= (ConditionTerm) CondTerm:termType AND CondFact:factType {: RESULT=new ConditionTerm(termType, factType); RESULT.setLine(termTypeleft); :}
				|
			 (BasicCondFact) CondFact:termType {: RESULT=new BasicCondFact(termType); RESULT.setLine(termTypeleft); :};						  
							
CondFact ::= (CondFactRelation) Expr:lType Relop:R1 Expr:rType {: RESULT=new CondFactRelation(lType, R1, rType); RESULT.setLine(lTypeleft); :}
				|
			 (CondFactNoRelation) Expr:type {: RESULT=new CondFactNoRelation(type); RESULT.setLine(typeleft); :};

Expr ::= (ExpressionAddList) TermExpr:t AddExprList:A1 {: RESULT=new ExpressionAddList(t, A1); RESULT.setLine(tleft); :}
			|
		 (ExpressionNoAddList) TermExpr:t {: RESULT=new ExpressionNoAddList(t); RESULT.setLine(tleft); :};

TermExpr ::= (TermExpression) Term:t {: RESULT=new TermExpression(t); RESULT.setLine(tleft); :}
				|
			 (MinusTermExpr) MINUS Term:t {: RESULT=new MinusTermExpr(t); RESULT.setLine(tleft); :};

AddExprList ::= (AddExpressionList) AddExprList:A1 Addop:A2 Term:t {: RESULT=new AddExpressionList(A1, A2, t); RESULT.setLine(A1left); :}
					|
				(SingleAddExpressionList) Addop:A1 Term:t {: RESULT=new SingleAddExpressionList(A1, t); RESULT.setLine(A1left); :};

Term ::= (TermMulopList) Factor:t MulFactor:M1 {: RESULT=new TermMulopList(t, M1); RESULT.setLine(tleft); :}
			|
		 (TermNoMulopList) Factor:t {: RESULT=new TermNoMulopList(t); RESULT.setLine(tleft); :};

MulFactor ::= (MulopFactor) MulFactor:M1 Mulop:M2 Factor:t {: RESULT=new MulopFactor(M1, M2, t); RESULT.setLine(M1left); :}
				|
			  (MulopFactorSingle) Mulop:M1 Factor:t {: RESULT=new MulopFactorSingle(M1, t); RESULT.setLine(M1left); :};

Factor ::= (DesignatorFactorParams) Designator:desObj LPAREN FactorDesPar:F1 {: RESULT=new DesignatorFactorParams(desObj, F1); RESULT.setLine(desObjleft); :}
			|
		   (DesignatorFactorNoParams) Designator:desObj {: RESULT=new DesignatorFactorNoParams(desObj); RESULT.setLine(desObjleft); :}   
			|
		   (ConstNumFactor) NUMBER:val {: RESULT=new ConstNumFactor(val); RESULT.setLine(valleft); :}
			|
		   (ConstCharFactor) CHAR:val {: RESULT=new ConstCharFactor(val); RESULT.setLine(valleft); :}
			|
		   (ConstBoolFactor) BOOL:val {: RESULT=new ConstBoolFactor(val); RESULT.setLine(valleft); :}
		    |
		   (AllocationExpr) NEW Type:t ArrRefAlloc:A1 {: RESULT=new AllocationExpr(t, A1); RESULT.setLine(tleft); :}
		    |
		   (ParenExpression) LPAREN Expr:type RPAREN {: RESULT=new ParenExpression(type); RESULT.setLine(typeleft); :};	

ArrRefAlloc ::= (ArrayRefListAlloc) ArrRefAlloc:A1 LSQU Expr:type RSQU {: RESULT=new ArrayRefListAlloc(A1, type); RESULT.setLine(A1left); :}
		     			|
		   		(NoArrayRefAlloc) {: RESULT=new NoArrayRefAlloc(); :}; /*epsilon*/		   	   		  

FactorDesPar ::= (FactorDesignatorParActs) ActPars:A1 RPAREN {: RESULT=new FactorDesignatorParActs(A1); RESULT.setLine(A1left); :}
					|
				 (NoFactorDesignatorParActs) RPAREN {: RESULT=new NoFactorDesignatorParActs(); :};

Designator ::= (DesignatorClass) Designator:desObj RefDot:R1 SimpleDesignator:simpleDesObj {: RESULT=new DesignatorClass(desObj, R1, simpleDesObj); RESULT.setLine(desObjleft); :}
				  |
			   (SimpleDesignatorClass) SimpleDesignator:desObj {: RESULT=new SimpleDesignatorClass(desObj); RESULT.setLine(desObjleft); :};

SimpleDesignator ::= (SimpleDesignator) DesName:name ArrRef:A1 {: RESULT=new SimpleDesignator(name, A1); RESULT.setLine(nameleft); :};

DesName ::= (DesName) IDENT:name {: RESULT=new DesName(name); RESULT.setLine(nameleft); :};

RefDot ::= (ReferencingDot) DOT {: RESULT=new ReferencingDot(); :};
	   
ArrRef ::= (ArrayRefList) ArrRef:A1 LSQU Expr:type RSQU {: RESULT=new ArrayRefList(A1, type); RESULT.setLine(A1left); :}
		     			|
		   (NoArrayRef) {: RESULT=new NoArrayRef(); :}; /*epsilon*/
												  
Assignop ::= (Assignop) EQUAL {: RESULT=new Assignop(); :};

Relop ::= (RelEqual) LEQL {: RESULT=new RelEqual(); :} 
			| 
		  (NotEqual) NEQL {: RESULT=new NotEqual(); :} 
		    | 
		  (GreatherThan) GRT {: RESULT=new GreatherThan(); :} 
		    |
		  (GreatherEqualThan) GRTE {: RESULT=new GreatherEqualThan(); :} 
		    | 
		  (LessThan) LT {: RESULT=new LessThan(); :} 
		    |
		  (LessEqualThan) LTE {: RESULT=new LessEqualThan(); :};
		  
Addop ::= (Addop) PLUS {: RESULT=new Addop(); :} 
			| 
		  (Subop) MINUS {: RESULT=new Subop(); :};
		  
Mulop ::= (Mulop) MUL {: RESULT=new Mulop(); :}  
			|
		  (Divop) DIV {: RESULT=new Divop(); :}
		    | 
		  (Moduop) MODUO {: RESULT=new Moduop(); :};