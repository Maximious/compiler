package rs.ac.bg.etf.pp1;

import java_cup.runtime.*;
import org.apache.log4j.*;

// import java.io.*;
import rs.ac.bg.etf.pp1.ast.*;

parser code {:
	
	boolean errorDetected = false;
	
	Logger log = Logger.getLogger(getClass());
   
   
    // slede redefinisani metodi za prijavu gresaka radi izmene teksta poruke
     
    public void report_fatal_error(String message, Object info) throws java.lang.Exception {
      done_parsing();
      report_error(message, info);
    }
  
    public void syntax_error(Symbol cur_token) {
        report_error("\nSintaksna greska", cur_token);
    }
  
    public void unrecovered_syntax_error(Symbol cur_token) throws java.lang.Exception {
        report_fatal_error("Fatalna greska, parsiranje se ne moze nastaviti", cur_token);
    }

    public void report_error(String message, Object info) {
    	errorDetected = true;
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.error(msg.toString());
    }
    
    public void report_info(String message, Object info) {
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.info(msg.toString());
    }
    
:}

scan with {:
	Symbol s = this.getScanner().next_token();
	if (s != null && s.value != null) 
		log.info(s.toString() + " " + s.value.toString());
	return s;
:}

terminal EQUAL, PLUS, MINUS, MUL, DIV, MODUO, LEQL, NEQL, GRT, GRTE, LT, LTE; 
terminal PROG, BREAK, CLASS, ENUM, ELSE, CONST, IF, DO, WHILE, NEW, RECORD;
terminal PRINT, READ, RETURN, VOID, EXTENDS, CONTINUE, THIS, SUPER, GOTO; 
terminal AND, OR, PP, MM, SEMI, COMMA, LPAREN, RPAREN, LBRACE; 
terminal RBRACE, COLON, DOT, LSQU, RSQU; 
terminal Integer NUMBER;
terminal String IDENT;
terminal Character CHAR;
terminal Boolean BOOL;

/*
nonterminal rs.etf.pp1.symboltable.concepts.Obj Program; 
nonterminal rs.etf.pp1.symboltable.concepts.Struct Type;
*/ 

nonterminal Program, ProgramMet, ProgName, DeclList, PossDeclLists, VarDeclList, VarDecl, VarList, Arr, Type;
nonterminal ConstDeclList, ConstList, ConstVal, RecDeclList, ClassDecl, ClassDeclSec, ClassDef, ClassDefSec, ClassDefThrd; 
nonterminal ConstructorDecl, ConstructorDeclSec, MethodDeclList, MethodDecl, MethodDeclSec, MethodDef;
nonterminal MethodTypeName, MethodRetType, FormPars, FormalParamDecl, StatementList, Statement, LabeledStatement; 
nonterminal Label, SingleStatement, Statements, PrintExpr, ElseStmt, DesStatement, RetExpr, FactorDesPar;
nonterminal DesignatorActions, Expr, TermExpr, AddExprList, Term, MulFactor, Factor; 
nonterminal ActPars, ActualParamList, Designator, DesignatorRight, DesignatorRef, Assignop, Relop, Addop, Mulop;
nonterminal Condition, ConditionList, CondFact, CondTerm, OrList, AndList, Matched, Unmatched;

Program ::= (ProgramDeclList) PROG ProgName:p DeclList LBRACE ProgramMet
				|
			(ProgramNoDeclList) PROG ProgName:p LBRACE ProgramMet;
			
ProgramMet ::= (ProgramMethodDeclarations) MethodDeclList RBRACE
				 |
			   (NoProgramMethodDeclarations) RBRACE;
		
ProgName ::= (ProgName) IDENT:pName;			

DeclList ::= (Declarations) DeclList PossDeclLists
				|
			 (Declaration) PossDeclLists;			 

PossDeclLists ::= (PossVarDeclarations) VarDecl				  
					|
				  (PossConstDeclarations) ConstDeclList
  					|
				  (PossRecDeclarations) RecDeclList		
					|
				  (PossClassDeclarations) ClassDecl;


VarDeclList ::= (VarDeclarations) VarDeclList VarDecl
					|
				(SingleVarDeclaration) VarDecl; 

VarDecl ::= (VarDeclaration) Type:varType VarList SEMI;									
 
VarList ::= (VarListClass) VarList COMMA IDENT:varName Arr
			  |
			(SingleVar) IDENT:varName Arr;
		 			
Arr   	::= (Array) LSQU RSQU
			  |
			(NoArrDecl);/* epsilon */	
			
Type ::= (Type) IDENT:typeName;	


ConstDeclList ::= (ConstantDeclarationsList) CONST Type:varType ConstList SEMI;
					
ConstList ::= (ConstListClass) ConstList COMMA IDENT:varName EQUAL ConstVal
				|			
			  (SingleConst) IDENT:varName EQUAL ConstVal;	
			  		
ConstVal ::= (ConstBool) BOOL
				|
			 (ConstNum) NUMBER
				|
			 (ConstChar) CHAR;		
			 
			 
RecDeclList ::= (RecordDeclarationsList) RECORD IDENT:varName LBRACE VarDeclList RBRACE; 				
		
				
ClassDecl ::= (ClassExtend) CLASS IDENT:nm EXTENDS Type:t LBRACE ClassDeclSec
				|
			  (ClassNoExtend) CLASS IDENT:nm LBRACE ClassDeclSec;
			  
ClassDeclSec ::= (ClassVarDeclaration)  VarDeclList ClassDef
					|			 			
				 (ClassNoVarDeclaration) ClassDef;
				 
ClassDef ::= (ClassDefinition) ClassDefSec RBRACE
				|
			 (NoClassDefinition) RBRACE;
			 			 
ClassDefSec ::= (ConstructorClass) LBRACE ConstructorDecl ClassDefThrd 
					|
				(NoConstructorClass) LBRACE ClassDefThrd;

ClassDefThrd ::= (ClassMethods) MethodDeclList RBRACE
					|
				 (NoClassMethods) RBRACE;
			 
ConstructorDecl ::= (ConstructorVarDeclaration) IDENT:nm LPAREN RPAREN VarDeclList LBRACE 
						|				
				  	(ConstructorNoVarDeclaration) IDENT:nm LPAREN RPAREN LBRACE;
				  	
ConstructorDeclSec ::= (ClassStatements) StatementList RBRACE
						 |
					   (EmptyClass) RBRACE;		  	