package rs.ac.bg.etf.pp1;

import java_cup.runtime.*;
import org.apache.log4j.*;

// import java.io.*;
import rs.ac.bg.etf.pp1.ast.*;

parser code {:
	
	boolean errorDetected = false;
	
	Logger log = Logger.getLogger(getClass());
   
   
    // slede redefinisani metodi za prijavu gresaka radi izmene teksta poruke
     
    public void report_fatal_error(String message, Object info) throws java.lang.Exception {
      done_parsing();
      report_error(message, info);
    }
  
    public void syntax_error(Symbol cur_token) {
        report_error("\nSintaksna greska", cur_token);
    }
  
    public void unrecovered_syntax_error(Symbol cur_token) throws java.lang.Exception {
        report_fatal_error("Fatalna greska, parsiranje se ne moze nastaviti", cur_token);
    }

    public void report_error(String message, Object info) {
    	errorDetected = true;
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.error(msg.toString());
    }
    
    public void report_info(String message, Object info) {
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.info(msg.toString());
    }
    
:}

scan with {:
	Symbol s = this.getScanner().next_token();
	if (s != null && s.value != null) 
		log.info(s.toString() + " " + s.value.toString());
	return s;
:}

terminal EQUAL, PLUS, MINUS, MUL, DIV, MODUO, LEQL, NEQL, GRT, GRTE, LT, LTE; 
terminal PROG, BREAK, CLASS, ENUM, ELSE, CONST, IF, DO, WHILE, NEW, RECORD;
terminal PRINT, READ, RETURN, VOID, EXTENDS, CONTINUE, THIS, SUPER, GOTO; 
terminal AND, OR, PP, MM, SEMI, COMMA, LPAREN, RPAREN, LBRACE; 
terminal RBRACE, COLON, DOT, LSQU, RSQU; 
terminal Integer NUMBER;
terminal String IDENT;
terminal Character CHAR;
terminal Boolean BOOL;

/*
nonterminal rs.etf.pp1.symboltable.concepts.Obj Program; 
nonterminal rs.etf.pp1.symboltable.concepts.Struct Type;
*/ 

nonterminal Program, ProgramMet, ProgName, DeclList, PossDeclLists, VarDeclList, VarDecl, VarList, Arr, Type;
nonterminal ConstDeclList, ConstList, ConstVal, RecDeclList, ClassDecl, ClassDeclSec, ClassDef, ClassDefSec, ClassDefThrd; 
nonterminal ConstructorDecl, ConstructorDeclSec, MethodDeclList, MethodDecl, MethodDeclSec, MethodDef;
nonterminal MethodTypeName, MethodRetType, FormPars, FormalParamDecl, StatementList, Statement, LabeledStatement; 
nonterminal Label, SingleStatement, Statements, PrintExpr, ElseStmt, DesStatement, RetExpr, FactorDesPar;
nonterminal DesignatorActions, Expr, TermExpr, AddExprList, Term, MulFactor, Factor; 
nonterminal ActPars, ActualParamList, Designator, DesignatorRight, DesignatorRef, Assignop, Relop, Addop, Mulop;
nonterminal Condition, ConditionList, CondFact, CondTerm, OrList, AndList, Matched, Unmatched, ExprP;

/*
precedence left PLUS, MINUS;
precedence left MUL, DIV, MODUO;
precedence left LPAREN, RPAREN;
*/
Program ::= (ProgramDeclList) PROG ProgName:p DeclList LBRACE ProgramMet
				|
			(ProgramNoDeclList) PROG ProgName:p LBRACE ProgramMet;

ProgramMet ::= (ProgramMethodDeclarations) MethodDeclList RBRACE
				 |
			   (NoProgramMethodDeclarations) RBRACE;
		
ProgName ::= (ProgName) IDENT:pName;

DeclList ::= (Declarations) DeclList PossDeclLists
				|
			 (Declaration) PossDeclLists;			 

PossDeclLists ::= (PossVarDeclarations) VarDecl				  
					|
				  (PossConstDeclarations) ConstDeclList
  					|
				  (PossRecDeclarations) RecDeclList		
					|
				  (PossClassDeclarations) ClassDecl;


VarDeclList ::= (VarDeclarations) VarDeclList VarDecl
					|
				(SingleVarDeclaration) VarDecl; 

VarDecl ::= (VarDeclaration) Type:varType VarList SEMI;									
 
VarList ::= (VarListClass) VarList COMMA IDENT:varName Arr
			  |
			(SingleVar) IDENT:varName Arr;
		 			
Arr   	::= (Array) LSQU RSQU
			  |
			(NoArrDecl);/* epsilon */	
			
Type ::= (Type) IDENT:typeName;	


ConstDeclList ::= (ConstantDeclarationsList) CONST Type:varType ConstList SEMI;
					
ConstList ::= (ConstListClass) ConstList COMMA IDENT:varName EQUAL ConstVal
				|			
			  (SingleConst) IDENT:varName EQUAL ConstVal;	
			  		
ConstVal ::= (ConstBool) BOOL
				|
			 (ConstNum) NUMBER
				|
			 (ConstChar) CHAR;		
			 
			 
RecDeclList ::= (RecordDeclarationsList) RECORD IDENT:varName LBRACE VarDeclList RBRACE; 				
		
				
ClassDecl ::= (ClassExtend) CLASS IDENT:nm EXTENDS Type:t LBRACE ClassDeclSec
				|
			  (ClassNoExtend) CLASS IDENT:nm LBRACE ClassDeclSec;
			  
ClassDeclSec ::= (ClassVarDeclaration)  VarDeclList ClassDef
					|			 			
				 (ClassNoVarDeclaration) ClassDef;
				 
ClassDef ::= (ClassDefinition) ClassDefSec RBRACE
				|
			 (NoClassDefinition) RBRACE;
			 			 
ClassDefSec ::= (ConstructorClass) LBRACE ConstructorDecl ClassDefThrd 
					|
				(NoConstructorClass) LBRACE ClassDefThrd;

ClassDefThrd ::= (ClassMethods) MethodDeclList RBRACE
					|
				 (NoClassMethods) RBRACE;
			 
ConstructorDecl ::= (ConstructorVarDeclaration) IDENT:nm LPAREN RPAREN VarDeclList LBRACE 
						|				
				  	(ConstructorNoVarDeclaration) IDENT:nm LPAREN RPAREN LBRACE;
				  	
ConstructorDeclSec ::= (ClassStatements) StatementList RBRACE
						 |
					   (EmptyClass) RBRACE;
					   
					   
MethodDeclList ::= (MethodDeclarations) MethodDeclList MethodDecl
					 |
				   (SingleMethodDecl) MethodDecl;
				   					
MethodDecl ::= (MethodDeclFormPars) MethodTypeName LPAREN FormPars RPAREN MethodDeclSec
				 |
			   (NoMethodDeclFormPars) MethodTypeName LPAREN RPAREN MethodDeclSec;

MethodDeclSec ::= (MethodDeclVars)  VarDeclList LBRACE MethodDef   
					|
				  (NoMethodDeclVars) LBRACE MethodDef;
				  
MethodDef ::= (MethodDefinition) StatementList RBRACE
				|
			  (NoMethodDefinition) RBRACE;
				
MethodTypeName ::= (MethodTypeName) MethodRetType IDENT:methName;

MethodRetType ::= (MethodReturnType) Type:retType
					|
				  (VoidClass) VOID; 


FormPars ::= (FormalParams) FormPars COMMA FormalParamDecl
				|
			 (SingleFormalParamDecl) FormalParamDecl;	
			 				
FormalParamDecl ::= (FormalParamDecl) Type IDENT:name Arr;


StatementList ::= (StatementListClass) StatementList Statement 
					|
				  (SingleStmt) Statement;

Statement ::= (SingleStatementClass) LabeledStatement
			  	|
			  (StatementGroup) Statements;		   		 		   		   				 
			  
LabeledStatement ::= (LabeledSingleStatement) Label SingleStatement
						|			  
					 (NonLabeledSingleStatement) SingleStatement;
					 
Label ::= (Label) IDENT:name COLON;		  
		  							  				
SingleStatement ::= (MatchedClass) Matched
						|
					(UnmatchedClass) Unmatched;

Matched ::= (DesignatorStatementMatched) DesStatement SEMI
				|
			(Break) BREAK SEMI
				|
			(Continue) CONTINUE SEMI			 
				|
			(Return) RETURN RetExpr  			  
		  	    |
	   		(Read) READ LPAREN Designator RPAREN SEMI
				| 
			(Print) PRINT LPAREN PrintExpr RPAREN SEMI
				| 
			(Goto) GOTO Label SEMI
				|
			(Do) DO Statement WHILE LPAREN ConditionList RPAREN SEMI
			  	| 			
			(IfElse) IF LPAREN ConditionList RPAREN Matched ELSE Matched
				|
			(ErrAssignment) Designator EQUAL error SEMI
	  		{: parser.log.debug("Uspesan oporavak od greske pri dodeli vrednosti."); :};
	  		
Unmatched ::= (If) IF LPAREN ConditionList RPAREN Statement
				| 
			  (IfElseUnmatched) IF LPAREN ConditionList RPAREN Matched ELSE Unmatched; 	  		

Statements ::= LBRACE StatementList RBRACE;

PrintExpr ::= (PrintConstant) Expr COMMA NUMBER
					 |
			  (NoPrintConstants) Expr;
			  			  						    
RetExpr ::= (ReturnExpression) Expr SEMI
			|
			(NoReturnExpression) SEMI;  			
		

DesStatement ::= (DesignatorStatement) Designator DesignatorActions;

DesignatorActions ::= (Assign) Assignop Expr
						|
					  (CallProc) LPAREN ActPars RPAREN
					  	|
					  (PlusPlus) PP
					  	|		
					  (MinusMinus)	MM;
					  
Expr ::=  ExprP;		
					  
ExprP ::= (ExpressionAddList) Term:t AddExprList
			|
		  (ExpressionNoAddList) Term:t;

TermExpr ::= (TermExpression) MINUS  
				|
			 (MinusTermExpr); /*epsilon*/
			 			
AddExprList ::= (AddExpressionList) AddExprList Addop Term:t
					|
				(SingleAddExpressionList) Addop Term:t;	 			
		 	
Term ::= (TermMulopList) Factor:t MulFactor
			|
		 (TermNoMulopList) Factor:t;
		 
MulFactor ::= (MulopFactor) MulFactor Mulop Factor:t;

Factor ::= (DesignatorFactorParams) Designator LPAREN FactorDesPar								   
			|
		   (DesignatorFactorNoParams) Designator								   
			|
		   (ConstNumFactor) NUMBER
			|
		   (ConstCharFactor) CHAR
			|
		   (ConstBoolFactor) BOOL
		    |
		   (AllocationExpr) NEW Type:t LSQU Expr RSQU
		    |
		   (AllocationNoExpr) NEW Type:t
		    |
		   (ParenExpression) LPAREN Expr RPAREN;
		   
FactorDesPar ::= (FactorDesignatorParActs) ActPars RPAREN
					|
				 (NoFactorDesignatorParActs) RPAREN;
				 	
			   		   			   
ActPars ::= (ActualParamListClass) Expr ActualParamList 
				| 
		    (SingleActualParam) Expr; 
			   			   
ActualParamList ::= (ActualParams) ActualParamList COMMA Expr
					  |
					(ActualParam) Expr;


Designator ::= (DesignatorReferencing) IDENT:name DesignatorRight
				 |
			   (DesignatorSimple) IDENT:name;

DesignatorRight ::= (DesignatorRightList) DesignatorRight DesignatorRef
						|
					(DesignatorRightSingle) DesignatorRef;

DesignatorRef ::= (DotReferencing) DOT IDENT:name
					  |
				  (ArrayReferencing) LSQU Expr RSQU;

Condition ::= (ConditionClass) CondTerm OrList
				|
			  (BasicCondTerm) CondTerm;
			  
OrList ::= (ConditionOrList) OrList OR CondTerm
			 |
		   (ConditionOrListSingle) OR CondTerm;			  
			  
CondTerm ::= (ConditionTerm) CondFact AndList
				|
			  (BasicCondFact) CondFact;			
			  
AndList ::= (AndListClass) AndList AND CondFact
				|
			(AndListSingle) AND CondFact;
							
CondFact ::= (CondFactRelation) Expr Relop Expr
				|
			 (CondFactNoRelation) Expr;	
			 			   					

Assignop ::= (Assignop) EQUAL;

Relop ::= (RelEqual) LEQL 
			| 
		  (NotEqual) NEQL 
		    | 
		  (GreatherThan) GRT 
		    |
		  (GreatherEqualThan) GRTE 
		    | 
		  (LessThan) LT 
		    |
		  (LessEqualThan) LTE;
		  
Addop ::= (Addop) PLUS 
			| 
		  (Subop) MINUS;
		  
Mulop ::= (Mulop) MUL  
			|
		  (Divop) DIV
		    | 
		  (Moduop) MODUO;


		  	